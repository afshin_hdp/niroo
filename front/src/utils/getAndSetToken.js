export const getToken = ()=>{
    const accessToken = localStorage.getItem("accessToken");
    if(accessToken)
        return  accessToken;
    else
        return false;
}
export const setToken = (token)=>{
    return localStorage.setItem("accessToken",token)
}
export const getRefreshToken = ()=>{
    const refreshToken = localStorage.getItem("refreshToken");
    if(refreshToken)
        return  refreshToken;
    else
        return false;
}
export const setRefreshToken = (refreshToken)=>{
    return localStorage.setItem("refreshToken",refreshToken)
}

export const setUserName = (userName)=>{
    return localStorage.setItem("userName",userName)
}

export const getUserName = ()=>{
    const userName = localStorage.getItem("userName");
    if(userName)
        return  userName;
    else
        return false;
}

export const removeTokens = ()=>{ //// remove both tokens
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken");
    localStorage.removeItem("userName");
}
