import axios from "axios";
import {} from "utils/getAndSetToken";
import { toast } from 'react-toastify'

const instance = axios.create();
instance.defaults.baseURL = process.env.REACT_APP_APIS_URL;
instance.defaults.headers.post['Content-Type'] = 'application/JSON';
// Add a request interceptor
instance.interceptors.request.use(function (config) {
    // Do something before request is sent
    config.timeout = 5500;
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
instance.interceptors.response.use( response=> {
    return response;
  }, async (error)=> {
    toast.error(error.response.data.message)
    return Promise.reject(error);
  });


  export default instance;