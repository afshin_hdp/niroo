import axios from "config/axios";

export const loginApi = (user) =>{
    return axios.post("login",user);
}
export const registerApi = (user) =>{
    return axios.post("register",user);
}

export const baseLineApi = (userName) =>{
    return axios.get("baseLine/"+userName);
}
export const setDataApi = (data) =>{
    return axios.post("setData",data);
}

export const billApi = (userName) =>{
    return axios.get("bill/"+userName);
}

export const getNotificationApi = () =>{
    return axios.get("get-notifications");
}

export const getObjectionApi = (userName) =>{
    return axios.get("get-objections/"+userName);
}
export const setObjectionApi = (objection) =>{
    return axios.post("set-objection",objection);
}
export const answerToObjectionApi = (objection) =>{
    return axios.post("answer-to-objection",objection);
}
export const saveFormulaApi = (obj) =>{
    return axios.post("formula",obj);
}
