import {
  Routes,
  Route,
  useLocation,
  Link
} from "react-router-dom";
import Login from "./pages/authPage/login";
import Dashboard from "./pages/dashboard";
import BaseLineCalculation from "./pages/dashboard/baseLineCalculation";
import ExpenseRate from "./pages/dashboard/expenseRate";
import Bill from "./pages/dashboard/bill";
import Annoucement from "./pages/dashboard/announcements";
import Claims from "./pages/dashboard/claims";
import Setting from "./pages/dashboard/settings";
import RawData from "./pages/dashboard/rawData";
//// admin
import ReportAdmin from "./pages/dashboard/reportAdmin";
import DifferentDistAdmin from "./pages/dashboard/differentDistAdmin";
import DifferentAggregators from "./pages/dashboard/differentAggregators";
import ClaimsAdmin from "./pages/dashboard/claimsAdmin";
import FormulaAdmin from "./pages/dashboard/formulaAdmin";
import RawDataAdmin from "./pages/dashboard/rawDataAdmin";
////distributor
import ReportDis from "./pages/dashboard/reportDistributor";
import SelectDistributor from "./pages/dashboard/selectDistributor";
import BaseLineDist from "./pages/dashboard/baseLineDist";
import ConsumeDist from "./pages/dashboard/consumeDist";
import CalculateDist from "./pages/dashboard/calculateDist";
import ClaimDist from "./pages/dashboard/claimDist";
import AnnouncementsDist from "./pages/dashboard/announcementsDist";
import RawDataDist from "./pages/dashboard/rawDataDist";
import SettingDist from "./pages/dashboard/settingDist";
import { useNavigate } from "react-router-dom"

import  "./App.css"
import {getToken} from "utils/getAndSetToken"
import {useState,useEffect} from "react"

function App() {
  const[isLoggin,setIsLoggin] = useState(false);
  let location = useLocation();
  const [accessToken,setAccessToken] = useState("")  
  
  useEffect(()=>{
    setAccessToken(getToken())
  },[getToken()])

  useEffect(()=>{
    if(accessToken){
      setIsLoggin(true)
    }
    else{
      setIsLoggin(false)
    }
  },[location, accessToken])
  
  return (
    <Routes>
      {
         isLoggin ? (accessToken === "aggregator" ? (
        <>
          <Route path="/" element={<Dashboard />}  />
          <Route path="/dashboard" element={<Dashboard />}  />
          <Route path="/calculate-baseLine" element={<BaseLineCalculation />}  />
          <Route path="/expenseRate" element={<ExpenseRate />}  />
          <Route path="/bill" element={<Bill />}  />
          <Route path="/announcements" element={<Annoucement />}  />
          <Route path="/objections" element={<Claims />}  />
          <Route path="/raw-data" element={<RawData />}  />
          <Route path="/settings" element={<Setting />}  />
          <Route path="*" element={<NoMatch />} />
        </>
        ): accessToken === "admin" ? (
          <>
            <Route path="/" element={<ReportAdmin />}  />
            <Route path="/dashboard" element={<ReportAdmin />}  />
            <Route path="/differentDist" element={<DifferentDistAdmin />}  />
            <Route path="/differentAggregators" element={<DifferentAggregators />}  />
            <Route path="/formula" element={<FormulaAdmin />}  />
            <Route path="/claims" element={<ClaimsAdmin />}  />
            <Route path="/rawData" element={<RawDataAdmin />}  />
          </>
        ) : (
          <>
            <Route path="/" element={<ReportDis />}  />
            <Route path="/dashboard" element={<ReportDis />}  />
            <Route path="/reportDis" element={<ReportDis />}  />
            <Route path="/selectDist" element={<SelectDistributor />}  />
            <Route path="/baseLineDist" element={<BaseLineDist />}  />
            <Route path="/consumeDist" element={<ConsumeDist />}  />
            <Route path="/calculateDist" element={<CalculateDist />}  />
            <Route path="/claimDist" element={<ClaimDist />}  />
            <Route path="/announcementsDist" element={<AnnouncementsDist />}  />
            <Route path="/rawDataDist" element={<RawDataDist />}  />
            <Route path="/setting" element={<SettingDist />}  />
          </>
        )
      ):(
        <>
          <Route path="/" element={<Login />}  />
          <Route path="/login" element={<Login />}  />
          <Route path="*" element={<NoMatch />} />
        </>
      )
    }
    </Routes>
  );
}

export default App;

function NoMatch() {
  return (
    <div style={{display:"flex",flexDirection:'column',alignItems:'center'}} >
      <h1>Page Not Found</h1>
      <p>
        <Link to="/">رفتن به داشبورد</Link>
      </p>
    </div>
  );
}
