import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import SwipeableDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import {Grid, Stack, Tooltip} from "@mui/material"
import ReportIcon from '@mui/icons-material/ChatOutlined';
import CalculateOutlinedIcon from '@mui/icons-material/CalculateOutlined';
import BatteryCharging80OutlinedIcon from '@mui/icons-material/BatteryCharging80Outlined';
import CreditScoreOutlinedIcon from '@mui/icons-material/CreditScoreOutlined';
import GavelOutlinedIcon from '@mui/icons-material/GavelOutlined';
import CampaignOutlinedIcon from '@mui/icons-material/CampaignOutlined';
import FeedOutlinedIcon from '@mui/icons-material/FeedOutlined';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import CompareArrowsIcon from '@mui/icons-material/CompareArrows';
import AutoAwesomeMotionIcon from '@mui/icons-material/AutoAwesomeMotion';
import LocationSearchingIcon from '@mui/icons-material/LocationSearching';
import CalculateIcon from '@mui/icons-material/Calculate'
import ExitToAppIcon from '@mui/icons-material/ExitToApp'
import Logo from "assets/images/logo.png"
import CustomLink from './customeLink'
import {getToken} from "utils/getAndSetToken"
import { useNavigate } from "react-router-dom"
import {removeTokens} from "utils/getAndSetToken"
import { getUserName } from 'utils/getAndSetToken';
const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: theme.spacing(0, 1),
  background : '#E5EBEB',
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  boxShadow : "none",
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(SwipeableDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    position:"absolute",
    ...(open && {
      ...openedMixin(theme),
      '& .MuiDrawer-paper': openedMixin(theme),      
    }),
    ...(!open && {
      ...closedMixin(theme),
      '& .MuiDrawer-paper': closedMixin(theme),
    }),
  }),
);
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.getElementById("headertxt").style.display = "none";
    document.getElementById("headertxt1").style.display = "none";
  } else {
    document.getElementById("headertxt").style.display = "block";
    document.getElementById("headertxt1").style.display = "block";
  }
}
export default function MiniDrawer(props) {
  let navigate = useNavigate();
  const [userName,setUserName] = React.useState("")

  const getNameOfUser = ()=>{
      setUserName(getUserName())
  }

  React.useEffect(()=>{
    getNameOfUser()
  },[])

  const {children, pageTitle} = props;
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const accessType = getToken()
  const exit = ()=>{
    removeTokens()    
    navigate("/")    
  }
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: '46px',
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Grid container justifyContent="space-between" alignItems="center" >              
            <Grid>
              <Typography variant="body1" noWrap component="div" color="black" id="headertxt" >
                {pageTitle}
              </Typography>
            </Grid>
            <Grid>
              <Typography variant="body1" noWrap component="div" color="black" id="headertxt1" >
                {accessType === 'distributer' ? "توزیع کننده" : accessType === 'admin' ? "توانیر" :  userName === "aggregator" ? " تجمیع کننده منطقه 1": ' تجمیع کننده منطقه2' }

              </Typography>
            </Grid>
            <Stack spacing={3} direction="row" alignItems="center" >
            <Tooltip title="خروج">
              <ExitToAppIcon onClick={()=>exit()} style={{cursor : "pointer"}} />
            </Tooltip>  
            </Stack> 
          </Grid>  
        </Toolbar>
      </AppBar>
      <Drawer variant="permanent" open={open}  onClick={handleDrawerClose} >
        <DrawerHeader>
          <div style={{width:300,paddingInlineStart:60}}  >
          {
            open && (
            <img src={Logo} style={{width:80}} alt="ja" />
          )
          }
          </div>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>          
        </DrawerHeader>
        <List>     
          { 
            accessType === "aggregator" && (
              <>
          <CustomLink  to="/">     
            <ListItem button key="report">              
                <ListItemIcon>
                  <Tooltip title="گزارش تجمیعی">
                    <ReportIcon />
                  </Tooltip>
                </ListItemIcon>              
              <ListItemText primary="گزارش تجمیعی" />              
            </ListItem>
          </CustomLink>
          <CustomLink  to="/calculate-baseLine">
            <ListItem button key="khatemabna">
              <ListItemIcon>
                <Tooltip title="محاسبه خط مبنا">
                  <CalculateOutlinedIcon />
                </Tooltip>
              </ListItemIcon>
              <ListItemText primary="محاسبه خط مبنا" />
              </ListItem>
          </CustomLink>
          <CustomLink  to="/expenseRate">
            <ListItem button key="mizanemasraf">
              <ListItemIcon>
                <Tooltip title="میزان مصرف">
                  <BatteryCharging80OutlinedIcon />
                </Tooltip>  
              </ListItemIcon>
              <ListItemText primary="میزان مصرف" />
            </ListItem>
          </CustomLink>  
          <CustomLink  to="/bill">
            <ListItem button key="mohasebateMali">
              <ListItemIcon>
                <Tooltip title="محاسبات مالی">
                  <CreditScoreOutlinedIcon />
                </Tooltip>
              </ListItemIcon>
              <ListItemText primary="محاسبات مالی" />
            </ListItem>
           </CustomLink> 
           <CustomLink  to="/objections">
            <ListItem button key="eterazat">
              <ListItemIcon>
                  <Tooltip title="اعتراضات">
                    <GavelOutlinedIcon />
                  </Tooltip>
                </ListItemIcon>
                <ListItemText primary="اعتراضات" />
              </ListItem>
            </CustomLink>
            {/* <CustomLink  to="/announcements">
              <ListItem button key="elanat">
                <ListItemIcon>
                  <Tooltip title="اعلانات">
                    <CampaignOutlinedIcon />
                  </Tooltip>
                </ListItemIcon>
                <ListItemText primary="اعلانات" />
              </ListItem>
            </CustomLink> */}
            <CustomLink  to="/raw-data">
              <ListItem button key="dadehayeKham">
                <ListItemIcon>
                  <Tooltip title="داده‌های‌خام">
                    <FeedOutlinedIcon />
                  </Tooltip>
                </ListItemIcon>
                <ListItemText primary="داده‌های‌خام" />
              </ListItem>
            </CustomLink>
            {/* <CustomLink  to="/settings">
              <ListItem button key="setting">
                <ListItemIcon>
                  <Tooltip title="تنظیمات">
                    <SettingsOutlinedIcon />
                  </Tooltip>  
                </ListItemIcon>
                <ListItemText primary="تنظیمات" />
              </ListItem>
            </CustomLink> */}
            </>
            )}
            {
            accessType === "admin" && (
            <>  
              <CustomLink  to="/">     
                <ListItem button key="report">              
                    <ListItemIcon>
                      <Tooltip title="گزارش تجمیعی">
                        <ReportIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="گزارش تجمیعی" />              
                </ListItem>
              </CustomLink>
              {/* <CustomLink  to="/differentDist">     
                <ListItem button key="differentDist">              
                    <ListItemIcon>
                      <Tooltip title="توزیع های مختلف">
                        <CompareArrowsIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="توزیع های مختلف" />              
                </ListItem>
              </CustomLink> */}
              <CustomLink  to="/differentAggregators">     
                <ListItem button key="differentAggregators">              
                    <ListItemIcon>
                      <Tooltip title="وضعیت توزیع کننده‌ها">
                        <AutoAwesomeMotionIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="وضعیت توزیع کننده‌ها" />              
                </ListItem>
              </CustomLink>
              <CustomLink  to="/claims">     
                <ListItem button key="claims">              
                    <ListItemIcon>
                      <Tooltip title="مشاهده اعتراض‌ها">
                        <GavelOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="مشاهده اعتراض‌ها" />              
                </ListItem>
              </CustomLink>
              <CustomLink  to="/formula">     
                <ListItem button key="formula">              
                    <ListItemIcon>
                      <Tooltip title="تغییرات فرمول محاسبه">
                        <CalculateIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="تغییرات فرمول محاسبه" />              
                </ListItem>
              </CustomLink>
              {/* <CustomLink  to="/rawData">     
                <ListItem button key="rawData">              
                    <ListItemIcon>
                      <Tooltip title="داده‌های خام">
                        <FeedOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="داده‌های خام" />              
                </ListItem>
              </CustomLink> */}
            </>  
            )}{
            accessType === "distributer" && (
            <>
              <CustomLink  to="/reportDis">     
                <ListItem button key="reportDis">              
                    <ListItemIcon>
                      <Tooltip title="گزارش تجمیعی">
                        <ReportIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="گزارش تجمیعی" />              
                </ListItem>
              </CustomLink>
              <CustomLink  to="/selectDist">     
                <ListItem button key="selectDist">              
                    <ListItemIcon>
                      <Tooltip title="انتخاب تجمیع کننده">
                        <LocationSearchingIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="انتخاب تجمیع کننده" />              
                </ListItem>
              </CustomLink>
              <CustomLink  to="/baseLineDist">     
                <ListItem button key="baseLineDist">              
                    <ListItemIcon>
                      <Tooltip title="محاسبه خط مبنا">
                        <CalculateOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="محاسبه خط مبنا" />              
                </ListItem>
              </CustomLink>
              <CustomLink  to="/consumeDist">     
                <ListItem button key="consumeDist">              
                    <ListItemIcon>
                      <Tooltip title="میزان مصرف">
                        <BatteryCharging80OutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="میزان مصرف" />              
                </ListItem>
              </CustomLink>
              {/* <CustomLink  to="/calculateDist">     
                <ListItem button key="calculateDist">              
                    <ListItemIcon>
                      <Tooltip title="محاسبات مالی">
                        <CreditScoreOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="محاسبات مالی" />              
                </ListItem>
              </CustomLink> */}
              <CustomLink  to="/claimDist">     
                <ListItem button key="claimDist">              
                    <ListItemIcon>
                      <Tooltip title="اعتراض‌ها">
                        <GavelOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="اعتراض‌ها" />              
                </ListItem>
              </CustomLink>
              {/* <CustomLink  to="/announcementsDist">     
                <ListItem button key="announcementsDist">              
                    <ListItemIcon>
                      <Tooltip title="اعلانات">
                        <CampaignOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="اعلانات" />              
                </ListItem>
              </CustomLink> */}
              {/* <CustomLink  to="/rawDataDist">     
                <ListItem button key="rawDataDist">              
                    <ListItemIcon>
                      <Tooltip title="داده‌های خام">
                        <FeedOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="داده‌های خام" />              
                </ListItem>
              </CustomLink> */}
              {/* <CustomLink  to="/setting">     
                <ListItem button key="setting">              
                    <ListItemIcon>
                      <Tooltip title="تنظیمات">
                        <SettingsOutlinedIcon />
                      </Tooltip>
                    </ListItemIcon>              
                  <ListItemText primary="تنظیمات" />              
                </ListItem>
              </CustomLink> */}
            </>
            )}
        </List>       
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1, p: 3,marginTop:8,background:'rgb(248,249,249,0.3)' }}>        
         {children}   
      </Box>
    </Box>
  );
}
