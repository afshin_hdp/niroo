import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import Table from "components/Table"
import {useState,useEffect} from "react"
import { getUserName } from "utils/getAndSetToken"
import { billApi } from "apis/api"
import { Button, Grid, TextField, Typography,Select , MenuItem } from "@mui/material"
import { setObjectionApi } from "apis/api"
import  Modal  from "components/Modal"
import { toast } from "react-toastify"
import PersianNumber from "components/PersianNumber"
const Bill = ()=>{
    const [userName,setUserName] = useState("")
    const [bills,setBills ] = useState({bill:'',finalBill:''})
    const getNameOfUser = ()=>{
        setUserName(getUserName())
    }
    useEffect(()=>{
        getNameOfUser()
    },[])
    useEffect(()=>{
        getBill()
    },[userName])
    const getBill = async()=>{
        try{
            const res = await billApi(userName)
            setBills({bill:res.data.bill,finalBill:res.data.finalBill})
        }catch(e){}
    }

    const [modal,setModal] = useState(false)
    
    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'وضعیت',field :'status'},
        {txt:'تاریخ',field :'date'},        
        {txt:'میزان صرفه‌جویی',field :'spend'},
        {txt:'قبض (به ریال)',field :'fee'},
        {txt:'اعلام اعتراض',field :'claim'},
    ]

    const tableRows = [
        {
            no : <PersianNumber number={1}  />,
            status : 'پرداخت‌شده',
            date : <PersianNumber number={'20 ماه'}  />,            
            spend : <PersianNumber number={bills.bill}  />,
            fee :  (bills.bill < 0 ?  
                <Typography style={{color:'tomato'}} > <PersianNumber number={bills.finalBill}  /> جریمه  </Typography> :
                <Typography style={{color:'teal'}} > <PersianNumber number={bills.finalBill}  />پاداش</Typography>
            ),
            claim : 
                <Grid container justifyContent={'space-between'} style={{width:220}} >
                    <Button onClick={()=>setModal(true)} color="error" variant="contained" >ثبت اعتراض</Button>
                </Grid>
        },
        {
            no : <Typography style={{color:'silver'}} ><PersianNumber number={'2 (فرضی)'}  /> </Typography>,
            status : 'پرداخت‌ نشده',
            date : <PersianNumber number={'21 ماه'}  />,            
            spend : <PersianNumber number={9800000}  />,
            fee : <PersianNumber number={1570}  />,
            claim : 
                <Grid container justifyContent={'space-between'} style={{width:220}} >
                    <Button disabled color="error" variant="contained" >ثبت اعتراض</Button>
                </Grid>        
        },
        {
            no : <Typography style={{color:'silver'}} > <PersianNumber number={'3 (فرضی)'}  /></Typography>,
            status : 'پرداخت‌ نشده',
            date : <PersianNumber number={'22 ماه'}  />,            
            spend : <PersianNumber number={1080000}  />,
            fee : <PersianNumber number={32870}  />,
            claim : 
                <Grid container justifyContent={'space-between'} style={{width:220}} >
                    <Button disabled color="error" variant="contained" >ثبت اعتراض</Button>
                </Grid>
        }
        ]
    
    const [claim,setClaim] = useState({
        title : '',
        text : ''
    })    
    const setObjection = async()=>{
        try{
            await setObjectionApi({
                subject : claim.title,
                msg : claim.text,
                bill : bills.finalBill,
                user : userName
            })
            setModal(false)
            toast.success('با موفقیت ثبت شد')
        }catch(e){}
    }
    return (
    <LayOut pageTitle="محاسبات مالی">
        <Modal open={modal} title="اضافه کردن اعتراض جدید" handleClose={()=>setModal(false)} >
            <Grid container  style={{width:350}} >
                <Grid xs={12} >
                    <Select
                        labelId="demo-simple-select-disabled-label"
                        id="demo-simple-select-disabled"
                        label="عنوان اعتراض"
                        style={{width:350}} 
                        onChange={(e)=>setClaim({...claim,title:e.target.value})}                       
                        >
                            <MenuItem style={{direction:'rtl'}} value={'اعتراض به مبلغ قابل پرداخت ثبت شده'}>اعتراض به مبلغ قابل پرداخت ثبت شده</MenuItem>
                            <MenuItem style={{direction:'rtl'}} value={'اعتراض به حجم مصرف ثبت شده'}>اعتراض به حجم مصرف ثبت شده</MenuItem>
                            <MenuItem style={{direction:'rtl'}} value={'اعتراض به تاریخ اعمال شده در محاسبه مالی'}>اعتراض به تاریخ اعمال شده در محاسبه مالی</MenuItem>
                            <MenuItem selected style={{direction:'rtl'}} value={'سایر'}>سایر</MenuItem>
                    </Select>    
                </Grid>
                <Grid xs={12} style={{marginTop:15}} >
                    <TextField 
                        onChange={(e)=>setClaim((prev)=>({...prev,text:e.target.value}))}
                        style={{width:350}}  
                        label="متن اعتراض" 
                        fullWidth 
                        multiline 
                        minRows={5} 
                        />
                </Grid>
                <Grid xs={12}  container item justifyContent={"space-around"} style={{marginTop:15}} >
                    <Button onClick={()=>setObjection()} variant="contained" color="success" >ثبت‌اعتراض </Button>
                    <Button onClick={()=>setModal(false)} variant="contained" color="error" >انصراف</Button>
                </Grid>
            </Grid>
        </Modal>
        <MyContainer>
            <BigCard title={"محاسبات مالی"} >
                <Table  rows={tableRows} cols={tableCols} />
            </BigCard>
        </MyContainer>
    </LayOut>
    )
}
export default Bill