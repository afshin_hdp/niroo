import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import { Grid,Card, CardContent, Divider, Typography } from "@mui/material"
import PersianNumber from "components/PersianNumber"
import { useEffect } from "react"
import { useState } from "react"
import { baseLineApi } from "apis/api"
import Chart from 'chart.js/auto';

const Distributor = ()=>{
    const[usage1,setUsage1] = useState({})
    const[usage2,setUsage2] = useState({})
    const[usageAll,setUsageAll] = useState({})
    useEffect(()=>{
        getAggregatorUsage()
    },[])
    const getAggregatorUsage = async()=>{
        try{
            const res1 = await baseLineApi('aggregator')
            setUsage1((res1.data))
            const res2 = await baseLineApi('aggregator2')
            setUsage2(res2.data)
            const resAll = await baseLineApi("all")
            setUsageAll(resAll.data)
        }catch(e){}
    }
    useEffect(()=>{
        if(Object.keys(usageAll).length > 0){
            const ctx = document.getElementById('usageAll');        
            const chart1 = new Chart(ctx, {
                type: 'line',
                data : {
                    labels: ['20', '21', '22', '23', '24', '25', '26'],
                    datasets: [{
                        label: 'میزان مصرف',
                        data: [usageAll.usage[20],usageAll.usage[21],usageAll.usage[22],
                            usageAll.usage[23],usageAll.usage[24],usageAll.usage[25],usageAll.usage[26]],
                        fill: true,
                        borderColor: 'rgb(75, 192, 192)',
                        },{
                            label: 'خط مبنا',
                            data: [usageAll.baseLine[20],usageAll.baseLine[21],usageAll.baseLine[22],
                                usageAll.baseLine[23],usageAll.baseLine[24],usageAll.baseLine[25],usageAll.baseLine[26]],
                            fill: true,
                            borderColor: 'tomato',
                            borderDash : [10,5]
                                    }
                    ]
                },
                options: {
                    aspectRatio:4,
                    scales: {
                        y: { // defining min and max so hiding the dataset does not change scale range
                        min: 0,
                        max: ((Math.max(usageAll.usage[20],usageAll.usage[21],usageAll.usage[22],usageAll.usage[23],usageAll.usage[24],usageAll.usage[25],
                            usageAll.usage[26]) + 10000) / 10000).toFixed() *10000                      
                        }
                    },
                    animations: {
                        tension: {
                        duration: 1000,
                        easing: 'linear',
                        from: 1,
                        to: 0,
                        loop: false
                        }
                    },
                }
            });
            return ()=>chart1.destroy()
        }
    },[usageAll])
    useEffect(()=>{
        if(Object.keys(usage1).length > 0){            
        const ctx = document.getElementById('usage1');
        const chart1 = new Chart(ctx, {
            type: 'line',
            data : {
                labels: ['20', '21', '22', '23', '24', '25', '26'],
                datasets: [{
                label: 'میزان مصرف',
                data: [usage1.usage[20],usage1.usage[21],usage1.usage[22],usage1.usage[23],usage1.usage[24],usage1.usage[25],usage1.usage[26]],
                fill: true,
                borderColor: 'rgb(75, 192, 192)',
                },
                {
                    label: 'خط مبنا',
                    data: [usage1.baseLine[20],usage1.baseLine[21],usage1.baseLine[22],usage1.baseLine[23],usage1.baseLine[24],usage1.baseLine[25],usage1.baseLine[26]],
                    borderColor: 'tomato',
                    borderDash : [10,5]
                    }                
                ]
            },
            options: {
                aspectRatio:4,
                scales: {
                    y: { // defining min and max so hiding the dataset does not change scale range
                    min: 0,
                    max: ((Math.max(usage1.usage[20],usage1.usage[21],usage1.usage[22],usage1.usage[23],usage1.usage[24],usage1.usage[25],
                        usage1.usage[26]) + 10000) / 1000).toFixed() *1000                      
                    }
                },
                animations: {
                    tension: {
                    duration: 1000,
                    easing: 'linear',
                    from: 1,
                    to: 0,
                    loop: false
                    }
                },
            }
        });
        return ()=>chart1.destroy()
    }
    },[usage1])

    useEffect(()=>{
        if(Object.keys(usage2).length > 0){
        const ctx2 = document.getElementById('usage2');
        const chart2 = new Chart(ctx2, {
            type: 'line',
            data : {
                labels: ['20', '21', '22', '23', '24', '25', '26'],
                datasets: [{
                label: 'مصرف',
                data: [usage2.usage[20],usage2.usage[21],usage2.usage[22],usage2.usage[23],usage2.usage[24],usage2.usage[25],usage2.usage[26]],                
                fill: true,
                borderColor: 'rgb(75, 192, 192)',
                },
                {
                    label: 'خط مبنا',
                    data: [usage2.baseLine[20],usage2.baseLine[21],usage2.baseLine[22],usage2.baseLine[23],usage2.baseLine[24],usage2.baseLine[25],usage2.baseLine[26]],                    
                    borderColor: 'tomato',
                    borderDash : [10,5]
                    }                
                ]
            },
            options: {
                aspectRatio:4,
                scales: {
                    y: { // defining min and max so hiding the dataset does not change scale range
                    min: 0,
                    max: 85000 
                    }
                },
                animations: {
                    tension: {
                    duration: 1000,
                    easing: 'linear',
                    from: 1,
                    to: 0,
                    loop: false
                    }
                },
            }
        });
        
        const ctx3 = document.getElementById('usage3');
        const chart3 = new Chart(ctx3, {
            type: 'line',
            data : {
                labels: ['1', '2', '3', '4', '5', '6', '7'],
                datasets: [{
                label: '',
                data: [45000,21500,9000,12500,28000,36000,55000],
                fill: true,
                borderColor: 'gray',
                }
                ]
            },
            options: {
                aspectRatio:4,
                scales: {
                    y: { // defining min and max so hiding the dataset does not change scale range
                    min: 0,
                    max: 85000  
                    }
                },
                animations: {
                    tension: {
                    duration: 1000,
                    easing: 'linear',
                    from: 1,
                    to: 0,
                    loop: false
                    }
                },
            }
        });
        const ctx4 = document.getElementById('usage4');
        const chart4 = new Chart(ctx4, {
            type: 'line',
            data : {
                labels: ['1', '2', '3', '4', '5', '6', '7'],
                datasets: [{
                label: '',
                data: [80000,85000,42000,51000,23000,11000,10500],
                fill: true,
                borderColor: 'gray',
                }
                ]
            },
            options: {
                aspectRatio:4,
                scales: {
                    y: { // defining min and max so hiding the dataset does not change scale range
                    min: 0,
                    max: 85000  
                    }
                },
                animations: {
                    tension: {
                    duration: 1000,
                    easing: 'linear',
                    from: 1,
                    to: 0,
                    loop: false
                    }
                },
            }
        });
        return ()=>{chart2.destroy();chart3.destroy();chart4.destroy()}
    }
    },[usage2])
    
    return (
        <LayOut pageTitle= "گزارش‌های تجمیعی">
            <MyContainer>
                <BigCard title={"گزارش‌های تجمیعی"} >
                <Grid container   spacing={3} style={{marginTop:15}} justifyContent="space-between" alignItems={"flex-start"} >
                    <Grid item xs={6}   >
                        <Typography variant="body1" component="div">
                            تجمیع کننده ۱
                            <Divider />
                        </Typography>                            
                            <canvas width="400" height="300" style={{marginTop:10}} id="usage1" ></canvas>
                    </Grid>        
                    <Grid item xs={6} >
                        <Typography variant="body1" component="div">
                            تجمیع کننده 2
                            <Divider />
                        </Typography>                            
                        <canvas width="400" height="300" style={{marginTop:10}} id="usage2" ></canvas>
                    </Grid>            
                    <Grid item xs={6} >
                        <Typography variant="body1" component="div">
                            تجمیع کننده 3(فرضی)
                            <Divider />
                        </Typography>                            
                        <canvas width="400" height="300" style={{marginTop:10}} id="usage3" ></canvas>
                    </Grid>            
                    <Grid item xs={6} >
                        <Typography variant="body1" component="div">
                            تجمیع کننده 4(فرضی)
                            <Divider />
                        </Typography>                            
                        <canvas width="400" height="300" style={{marginTop:10}} id="usage4" ></canvas>
                    </Grid>            
                </Grid>
                </BigCard>
                <BigCard title={"میزان کل مصرف"} >
                    <canvas width="500" height="450" style={{marginTop:10}} id="usageAll" ></canvas>
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default Distributor