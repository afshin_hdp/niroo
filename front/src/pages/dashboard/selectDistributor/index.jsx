import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import Table from "components/Table"
import PersianNumber from "components/PersianNumber"
import { Button, Typography, Grid , Backdrop } from "@mui/material"
import { baseLineApi } from "apis/api"
import { useEffect,useState } from "react"
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box'
import Modal from "components/Modal"
import { billApi } from "apis/api"
const SelectDistributor = ()=>{
    const [totConsume,settotConsume] = useState({all:0,ag1:0,ag2:0,ag3:1250,ag4:2540})
    const [loading,setLoading] = useState(false)
    const [modal,setModal] = useState(false)
    const [selectedAg,setSelectedAg] = useState(0)
    const [modalLoading,setModalLoading] = useState(false)
    const [selectedRows,setSelectedRows] = useState([])

    useEffect(()=>{
        getConsumes()
    },[])
    const getConsumes = async()=>{
        try{
            setLoading(true)
            const res1 = await baseLineApi("aggregator")
            const res2 = await baseLineApi("aggregator2")
            let sum1 = 0
            let sum2 = 0
            for (let c in res1.data.usage){
                sum1 += Number(res1.data.usage[c])
            }
            for (let c in res2.data.usage){
                sum2 += Number(res2.data.usage[c])
            }

            settotConsume(prev=>({...prev,ag1:sum1,ag2:sum2}))   
        }catch(e){}
        finally{
            setLoading(false)
        }
    } 
    const [bills,setBills ] = useState({bill:'',finalBill:''})

    useEffect(()=>{
        if(selectedAg !== 0)
        getBill()
    },[selectedAg])

    const getBill = async()=>{        
        try{
            setModalLoading(true)
            console.log(selectedAg)
            const res = await billApi(selectedAg === "ag1" ? 'aggregator' : 'aggregator2' )
            setBills(prev =>({...prev, bill:res.data.bill,finalBill:res.data.finalBill}))
        }catch(e){}
        finally{
            setModalLoading(false)
        }
    }

    const openModal = (ag)=>{
        setSelectedAg(ag)
        setModal(true)
    }
    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'نام تجمیع کننده',field :'name'},
        {txt:'میزان مصرف هفتگی (KWh)',field :'weekly'},
        {txt:'میزان مصرف ماهانه (KWh)',field :'monthly'},        
        {txt:'عملیات',field :'action'},        
    ]
    useEffect(()=>{
        setSelectedRows(
            [
                {
                    no : <PersianNumber number={1}  />,
                    status : 'پرداخت‌شده',
                    date : <PersianNumber number={'20 ماه'}  />,            
                    spend : <PersianNumber number={bills.bill}  />,
                    fee :  (bills.bill < 0 ?  
                        <Typography style={{color:'tomato'}} > <PersianNumber number={bills.finalBill}  /> جریمه  </Typography> :
                        <Typography style={{color:'teal'}} > <PersianNumber number={bills.finalBill}  />پاداش</Typography>
                    )
                },
                {
                    no : <Typography style={{color:'silver'}} ><PersianNumber number={'2 (فرضی)'}  /> </Typography>,
                    status : 'پرداخت‌ نشده',
                    date : <PersianNumber number={'21 ماه'}  />,            
                    spend : <PersianNumber number={9800000}  />,
                    fee : <PersianNumber number={1570}  />,
                },
                {
                    no : <Typography style={{color:'silver'}} > <PersianNumber number={'3 (فرضی)'}  /></Typography>,
                    status : 'پرداخت‌ نشده',
                    date : <PersianNumber number={'22 ماه'}  />,            
                    spend : <PersianNumber number={1080000}  />,
                    fee : <PersianNumber number={32870}  />,
                }
                ]
        )    
    },[bills])
    const selectedCols = [
        {txt:'شماره',field :'no'},
        {txt:'وضعیت',field :'status'},
        {txt:'تاریخ',field :'date'},        
        {txt:'میزان صرفه‌جویی',field :'spend'},
        {txt:'قبض (به ریال)',field :'fee'}
    ]
    
    
    const tableRows = [
        {
            no : <PersianNumber number={1}  />,
            name : 
                <Typography>
                    <PersianNumber number={'تجمیع کننده ۱'} />
                </Typography> ,
            weekly: 
                <Box sx={{ display: 'flex' }}>
                    { loading ? <CircularProgress style={{height:20,width:20}} color="secondary" /> : <PersianNumber number={totConsume.ag1} /> }
                </Box>,
            monthly :                
            <Box sx={{ display: 'flex' }}>
                { loading ? <CircularProgress style={{height:20,width:20}} color="secondary" /> : <PersianNumber number={totConsume.ag1} /> }
            </Box>,
            action : <Button onClick={()=>openModal('ag1')} variant="contained" color="info" >صفحه تجمیع‌کننده</Button>
        },
        {
            no : <PersianNumber number={2}  />,
            name : 
                <Typography>
                    <PersianNumber number={'تجمیع کننده 2'} />
                </Typography> ,
            weekly:
                <Box sx={{ display: 'flex' }}>
            { loading ? <CircularProgress style={{height:20,width:20}} color="secondary" /> : <PersianNumber number={totConsume.ag2} /> }
                </Box>,
            monthly :
                <Box sx={{ display: 'flex' }}>
                    { loading ? <CircularProgress style={{height:20,width:20}} color="secondary" /> : <PersianNumber number={totConsume.ag2} /> }
                </Box>,
            action : <Button variant="contained" onClick={()=>openModal('ag2')} color="info" >صفحه تجمیع‌کننده</Button>    
        },
        {
            no : <PersianNumber number={'3  (فرضی)'}  />,
            name : 
                <Typography>
                    <PersianNumber number={'تجمیع کننده 3'} />
                </Typography> ,
            weekly:totConsume.ag3,
            monthly :totConsume.ag3,    
            action : <Button variant="contained" disabled color="info" >صفحه تجمیع‌کننده</Button>
        },
        {
            no : <PersianNumber number={"4  (فرضی)"}  />,
            name : 
                <Typography>
                    <PersianNumber number={'تجمیع کننده 4'} />
                </Typography> ,
            weekly:totConsume.ag4,
            monthly :totConsume.ag4,    
            action : <Button variant="contained" disabled  color="info" >صفحه تجمیع‌کننده</Button>
        }
    ]


    return (
        <LayOut pageTitle= "انتخاب تجمیع کننده">
            <Modal open={modal}  handleClose={()=>{setModal(false);setSelectedAg(0)}}  title="تجمیع کننده " >                
                <Backdrop
                    sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={modalLoading}
                    >
                    <CircularProgress color="inherit" />
                </Backdrop>
                <Table rows={selectedRows} cols={selectedCols} />
                <Grid style={{marginTop:12}} container justifyContent={"flex-end"} >
                    <Button color="success" variant="contained" onClick={()=>{setModal(false);setSelectedAg(0)}} >بستن</Button>
                </Grid>
            </Modal>
            <MyContainer>
                <BigCard title={"انتخاب تجمیع کننده"} >
                    <Table rows={tableRows} cols={tableCols} />
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default SelectDistributor