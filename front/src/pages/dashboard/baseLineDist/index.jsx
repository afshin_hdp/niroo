import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import PersianNumber from "components/PersianNumber"
import { Chip,Typography,Stack,Divider,Tabs,Tab, Box ,Grid} from "@mui/material"
import PropTypes from 'prop-types';
import React from "react"
import { useEffect } from "react"
import { baseLineApi } from "apis/api"
import { useState } from "react"
import Chart from 'chart.js/auto';

const BaseLineDist = ()=>{
    const [value, setValue] = React.useState(0);
    const [baseLine1,setBaseLine1] = useState({})
    const [baseLine2,setBaseLine2] = useState({})

    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
    function TabPanel(props) {
        const { children, value, index, ...other } = props;
      
        return (
          <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
          >
            {value === index && (
              <Box sx={{ p: 3 }}>
                <Typography>{children}</Typography>
              </Box>
            )}
          </div>
        );
      }
      
      TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.number.isRequired,
        value: PropTypes.number.isRequired,
      };
      
      function a11yProps(index) {
        return {
          id: `simple-tab-${index}`,
          'aria-controls': `simple-tabpanel-${index}`,
        };
      }
      //// 
      useEffect(()=>{
        getAggregatorsData()
      },[])
      const getAggregatorsData = async()=>{
          try{
            const res1 = await baseLineApi("aggregator")
            setBaseLine1(res1.data)
          }catch(e){}
      }

      useEffect(()=>{
        if (value === 0)
            getAggregatorsData()
        else if(value === 1)
            get2ndBaseLineData()
      },[value])
      const get2ndBaseLineData = async()=>{
          try{
            const res2 = await baseLineApi("aggregator2")
            setBaseLine2(res2.data)
          }catch(e){}
      }
      useEffect(()=>{
        if(Object.keys(baseLine1).length > 0 ){  
        const ctx = document.getElementById('aggregator1');
        const weeklyChart = new Chart(ctx, {
            type: 'line',
            data : {
                labels: ['20', '21', '22', '23', '24', '25', '26'],
                datasets: [{
                label: 'خط مبنا',
                data: baseLine1.baseLine ?  [baseLine1.baseLine[20],baseLine1.baseLine[21],baseLine1.baseLine[22],
                baseLine1.baseLine[23],baseLine1.baseLine[24],baseLine1.baseLine[25],
                baseLine1.baseLine[26]] : [],
                fill: true,
                borderColor: 'rgb(75, 192, 192)',
                },
                {
                    label: 'مصرف',
                    data: baseLine1.usage ?  [baseLine1.usage[20],baseLine1.usage[21],baseLine1.usage[22],
                    baseLine1.usage[23],baseLine1.usage[24],baseLine1.usage[25],
                    baseLine1.usage[26]] : [],
                    fill: true,
                    borderColor: 'gray',                
                }]
            },
            options: {
                aspectRatio:4,
                scales: {
                    y: { // defining min and max so hiding the dataset does not change scale range
                    min: 0,
                    max: Math.max (Math.max(baseLine1.usage[20],baseLine1.usage[21],baseLine1.usage[22],
                        baseLine1.usage[23],baseLine1.usage[24],baseLine1.usage[25],
                        baseLine1.usage[26]),Math.max(
                        baseLine1.baseLine[20],baseLine1.baseLine[21],baseLine1.baseLine[22],
                        baseLine1.baseLine[23],baseLine1.baseLine[24],baseLine1.baseLine[25],
                        baseLine1.baseLine[26]
                        )) + 1000
                    }
                },
                animations: {
                    tension: {
                    duration: 1000,
                    easing: 'linear',
                    from: 1,
                    to: 0,
                    loop: false
                    }
                },
            }
        })
        return ()=>weeklyChart.destroy();
        }
      },[baseLine1])
      useEffect(()=>{
        if(Object.keys(baseLine2).length > 0 ){  
            const ctx2 = document.getElementById('aggregator2');
            const weeklyChart2 = new Chart(ctx2, {
                type: 'line',
                data : {
                    labels: ['20', '21', '22', '23', '24', '25', '26'],
                    datasets: [{
                    label: 'خط مبنا',
                    data: baseLine2.baseLine ?  [baseLine2.baseLine[20],baseLine2.baseLine[21],baseLine2.baseLine[22],
                    baseLine2.baseLine[23],baseLine2.baseLine[24],baseLine2.baseLine[25],
                    baseLine2.baseLine[26]] : [],
                    fill: true,
                    borderColor: 'rgb(75, 192, 192)',
                    },
                    {
                        label: 'مصرف',
                        data: baseLine2.usage ?  [baseLine2.usage[20],baseLine2.usage[21],baseLine2.usage[22],
                        baseLine2.usage[23],baseLine2.usage[24],baseLine2.usage[25],
                        baseLine2.usage[26]] : [],
                        fill: true,
                        borderColor: 'gray',                
                    }]
                },
                options: {
                    aspectRatio:4,
                    scales: {
                        y: { // defining min and max so hiding the dataset does not change scale range
                        min: 0,
                        max: Math.max (Math.max(baseLine2.usage[20],baseLine2.usage[21],baseLine2.usage[22],
                            baseLine2.usage[23],baseLine2.usage[24],baseLine2.usage[25],
                            baseLine2.usage[26]),Math.max(
                                baseLine2.baseLine[20],baseLine2.baseLine[21],baseLine2.baseLine[22],
                                baseLine2.baseLine[23],baseLine2.baseLine[24],baseLine2.baseLine[25],
                                baseLine2.baseLine[26]
                            )) + 1000
                        }
                    },
                    animations: {
                        tension: {
                        duration: 1000,
                        easing: 'linear',
                        from: 1,
                        to: 0,
                        loop: false
                        }
                    },
                }
            })
            return ()=>weeklyChart2.destroy();
            }

      },[baseLine2])
      ////
    return (
        <LayOut pageTitle= "محاسبه خط مبنا">
            <MyContainer>
                <BigCard title={"محاسبه خط مبنا"} >
                <Stack style={{minHeight:120}} direction="row" spacing={1} justifyContent="space-around" alignItems="center" >
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography  >
                            دمای هوای امروز                         
                        </Typography>
                        <Chip label={<PersianNumber number={32+" سانتی‌گراد"} />} variant="filled" />
                    </Stack>    
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography>
                        رطوبت هوای امروز                        
                        </Typography>
                        <Chip label={<PersianNumber number={50+"%"} />} variant="filled" />
                    </Stack>    
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography>
                        وضعیت کلی هوا                       
                         </Typography>
                        <Chip label="آفتابی" variant="filled" />
                    </Stack>    
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography>
                        میزان تولید امروز                         
                        </Typography>
                        <Chip label={<PersianNumber number={675000 +" (kwh)"} /> } variant="filled" />
                    </Stack>    
                </Stack>  
                <Stack  >
                    <Typography variant="h6"  >
                       وزن شاخص‌ها (امروز)
                    </Typography>
                    <Divider />
                    <Stack style={{minHeight:120}} direction="row" spacing={1} justifyContent="space-around" alignItems="center" >
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            شاخص اول                      
                            </Typography>
                            <Chip label={<PersianNumber number={25} /> } variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            شاخص دوم                      
                            </Typography>
                            <Chip label={<PersianNumber number={225} /> } variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            شاخص سوم                      
                            </Typography>
                            <Chip label={<PersianNumber number={325} /> } variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            شاخص چهارم                      
                            </Typography>
                            <Chip label={<PersianNumber number={125} /> } variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            شاخص پنجم                      
                            </Typography>
                            <Chip label={<PersianNumber number={98} /> } variant="filled" />
                        </Stack>    
                    </Stack>    
                </Stack>  
            </BigCard>
            <BigCard title="" >
                <Box sx={{ width: '100%' }}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs 
                            value={value} 
                            onChange={handleChange} 
                            textColor="secondary"
                            indicatorColor="secondary"
                        >
                            <Tab label="تجمیع کننده ۱" {...a11yProps(0)} />
                            <Tab label="تجمیع کننده ۲" {...a11yProps(1)} />
                            <Tab label="تجمیع کننده ۳" {...a11yProps(2)} />
                            <Tab label="تجمیع کننده ۴" {...a11yProps(3)} />
                        </Tabs>
                    </Box>
                    <Grid style={{marginTop:12}} container justifyContent={"center"} alignItems={"center"} >
                        <Typography color={"error"} >
                    توجه شود که خط مبنای نشان داده شده تقریبی از خط مبناست.
                        </Typography>
                    </Grid>
                    <TabPanel value={value} index={0}>
                        <canvas style={{marginTop:10}} id="aggregator1" width="500" height="400"></canvas>
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <canvas style={{marginTop:10}} id="aggregator2" width="500" height="400"></canvas>
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        <canvas style={{marginTop:10}} id="aggregator3" width="500" height="400"></canvas>
                    </TabPanel>
                    <TabPanel value={value} index={3}>
                        <canvas style={{marginTop:10}} id="aggregator4" width="500" height="400"></canvas>
                    </TabPanel>
                </Box>
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default BaseLineDist