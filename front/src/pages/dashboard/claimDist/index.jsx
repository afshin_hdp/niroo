import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import {MenuItem, Select, Grid, InputLabel,Button, Backdrop, CircularProgress,Typography} from "@mui/material" 
import Table from "components/Table"
import { getObjectionApi } from "apis/api"
import PersianNumber from "components/PersianNumber"
import { useState,useEffect } from "react"
import Modal from "components/Modal"
import { Stack, TextField,Chip} from "@mui/material"

const ClaimDist = ()=>{
    const [objections,setObjections ] = useState([])
    const [userName,setUserName] = useState("")
    const [modal,setModal] = useState(false)
    const [message,setMessage] = useState({})
    const [loading,setLoading] = useState(false)
    const handleChange = (value)=>{
        setUserName(value)
    }

    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'عنوان',field :'subject'},        
        {txt:'قبض',field :'bill'},
        {txt:'وضعیت',field :'situation'},
        {txt:'بررسی',field :'details'},
    ]

    useEffect(()=>{
        if(userName !== "")
        getObjections()
    },[userName])
    const getObjections = async()=>{
        try{
            setLoading(true)
            const res = await getObjectionApi(userName)            
            const arr = res.data.obj.map((ob,i)=>(
                {
                    no:<PersianNumber number={++i}  />,
                    subject:ob.subject,
                    bill:<PersianNumber number={ob.bill}  />,
                    situation : ob.situation === "" ?
                    <Typography color="silver"  variant="contained" >بررسی نشده</Typography> 
                    : ob.situation ? 
                    <Typography color="secondary"  variant="contained" >تایید شده</Typography>
                    :
                    <Typography color="error"  variant="contained" >رد شده</Typography>,
                    details : <Button 
                        color="info" 
                        onClick={()=>{setModal(true);setMessage(ob)}}  
                        variant="contained" >
                         نمایش متن اعتراض
                         </Button>
                })
                )
            setObjections(arr)
        }catch(e){}
        finally{
            setLoading(false)
        }
    }


    return (
        <LayOut pageTitle= "اعتراض‌ها">
            <Modal title="پیغام" open={modal} handleClose={()=>{setModal(false);setMessage("")}} >
            <Grid xs={12}  container item direction={"column"} justifyContent={"space-around"} style={{marginTop:15}} >
                <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            عنوان اعتراض                       
                         </Typography>
                        <Chip label={message.subject} variant="filled" />
                    </Stack>    
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            متن اعتراض                       
                         </Typography>
                        <TextField 
                            minRows={4}
                            multiline 
                            value={message.msg} 
                            maxRows={4} 
                            />
                    </Stack>    
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            پاسخ اعتراض                       
                         </Typography>
                        <TextField 
                            minRows={4}
                            multiline                              
                            maxRows={4} 
                            value={message.answer}
                            />
                    </Stack>    
                </Grid>
                <Grid style={{width:350,marginTop:8}} container alignItems={"center"} justifyContent={'flex-end'} >
                    <Button variant="contained" color="info" onClick={()=>{setModal(false);setMessage({})}} >
                        بستن
                    </Button>
                </Grid>
            </Modal>
            <MyContainer>
                <BigCard title={"اعتراض‌ها"} >
                <Grid alignItems={"center"}  item container justifyContent={"flex-start"}  >
                <InputLabel style={{marginTop:8}} >انتخاب تجمیع کننده: </InputLabel>
                <Select
                    style={{width:350,marginTop:15}}
                    onChange={(e)=>handleChange(e.target.value)}
                    label="انتخاب تجمیع کننده"
                    placeholder="انتخاب تجمیع کننده"
                    color="primary"
                    >
                    <MenuItem style={{direction:"rtl"}} value={'aggregator'}>تجمیع کننده 1</MenuItem>
                    <MenuItem style={{direction:"rtl"}} value={'aggregator2'}>تجمیع کننده 2</MenuItem>
                    <MenuItem style={{direction:"rtl"}} value={'all'}>همه تجمیع کننده‌ها</MenuItem>
                </Select>
                </Grid>
                </BigCard>
                <BigCard title={"لیست اعتراضات"} >
                <Backdrop
                    sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={loading}                    
                    >
                    <CircularProgress color="inherit" />
                </Backdrop>
                  {
                      objections.length > 0 && (
                           <Table cols={tableCols} rows={objections}  /> 
                      )
                  }
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default ClaimDist