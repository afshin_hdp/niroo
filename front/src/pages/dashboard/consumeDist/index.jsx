import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import {Chip,Typography , Stack} from "@mui/material"
import PersianNumber from "components/PersianNumber"
import { useEffect } from "react"
import { baseLineApi } from "apis/api"
import { useState } from "react"
import { Chart } from "chart.js"
const ConsumeDist = ()=>{
    const [totConsume,settotConsume] = useState({all:0,ag1:0,ag2:0,ag3:1250,ag4:2540})
    const[usageAll,setUsageAll] = useState({})

    useEffect(()=>{
        getConsumes()
    },[])
    
    const getConsumes = async()=>{
        try{
            const totRes  = await baseLineApi("all")
            const res1 = await baseLineApi("aggregator")
            const res2 = await baseLineApi("aggregator2")
            let sum = 0
            let sum1 = 0
            let sum2 = 0
            for (let c in totRes.data.usage){
                sum += Number(totRes.data.usage[c])
            }
            for (let c in res1.data.usage){
                sum1 += Number(res1.data.usage[c])
            }
            for (let c in res2.data.usage){
                sum2 += Number(res2.data.usage[c])
            }
            setUsageAll(totRes.data)

            settotConsume(prev=>({...prev,ag1:sum1,ag2:sum2,all:sum}))   
        }catch(e){}
    } 
    useEffect(()=>{
        if(Object.keys(usageAll).length > 0){
            const ctx = document.getElementById('usageAll');        
            const chart1 = new Chart(ctx, {
                type: 'line',
                data : {
                    labels: ['20', '21', '22', '23', '24', '25', '26'],
                    datasets: [{
                        label: 'میزان مصرف',
                        data: [usageAll.usage[20],usageAll.usage[21],usageAll.usage[22],
                            usageAll.usage[23],usageAll.usage[24],usageAll.usage[25],usageAll.usage[26]],
                        fill: true,
                        borderColor: 'rgb(75, 192, 192)',
                        },{
                            label: 'خط مبنا',
                            data: [usageAll.baseLine[20],usageAll.baseLine[21],usageAll.baseLine[22],
                                usageAll.baseLine[23],usageAll.baseLine[24],usageAll.baseLine[25],usageAll.baseLine[26]],
                            fill: true,
                            borderColor: 'tomato',
                            borderDash : [10,5]
                                    }
                    ]
                },
                options: {
                    aspectRatio:4,
                    scales: {
                        y: { // defining min and max so hiding the dataset does not change scale range
                        min: 0,
                        max: ((Math.max(usageAll.usage[20],usageAll.usage[21],usageAll.usage[22],usageAll.usage[23],usageAll.usage[24],usageAll.usage[25],
                            usageAll.usage[26]) + 10000) / 10000).toFixed() *10000                      
                        }
                    },
                    animations: {
                        tension: {
                        duration: 1000,
                        easing: 'linear',
                        from: 1,
                        to: 0,
                        loop: false
                        }
                    },
                }
            });
            return ()=>chart1.destroy()
        }
    },[usageAll])

    return (
        <LayOut pageTitle= "میزان مصرف">
            <MyContainer>
                <BigCard title={"میزان مصرف"} >
                    <Stack style={{marginTop:12}} direction="column" spacing={3} 
                        justifyContent="center" 
                        alignItems="flex-start" >
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography  >
                                مصرف کلی تا به امروز                         
                            </Typography>
                            <Chip label={<PersianNumber number={totConsume.all+" KWh"} />} variant="filled" />
                        </Stack>    
                    </Stack>
                    <Stack style={{marginTop:12}} direction="row" spacing={3} 
                        justifyContent="center" 
                        alignItems="flex-start" >
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography  >
                            تجمیع کننده ۱                         
                            </Typography>
                            <Chip label={<PersianNumber number={totConsume.ag1+" KWh"} />} variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography  >
                            تجمیع کننده ۲                        
                            </Typography>
                            <Chip label={<PersianNumber number={totConsume.ag2+" KWh"} />} variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography  >
                            تجمیع کننده ۳                         
                            </Typography>
                            <Chip label={<PersianNumber number={totConsume.ag3+" KWh"} />} variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography  >
                            تجمیع کننده ۴                         
                            </Typography>
                            <Chip label={<PersianNumber number={totConsume.ag4+" KWh"} />} variant="filled" />
                        </Stack>    
                    </Stack>
                </BigCard>
                <BigCard title={"میزان کل مصرف"} >
                    <canvas width="500" height="450" style={{marginTop:10}} id="usageAll" ></canvas>
                </BigCard>

            </MyContainer>
        </LayOut>
        )
}
export default ConsumeDist