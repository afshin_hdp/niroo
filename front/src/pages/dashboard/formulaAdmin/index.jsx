import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import {Stack, Chip, Typography, Grid, TextField, Button, Select, MenuItem,InputLabel} from "@mui/material"
import PersianNumber from "components/PersianNumber"
import { useState } from "react"
import { saveFormulaApi } from "apis/api"
const FormulaAdmin = ()=>{
    const[mabna,setMabna] = useState({days:1,fromDays:1,aggregator:''})

    const save = async()=>{
        try{
            await saveFormulaApi({
                aggregator : mabna.aggregator,
                formula1 : mabna.days,
                formula2 : mabna.fromDays
            })
        }catch(e){}
    }
    return (
        <LayOut pageTitle= "تغییرات فرمول محاسبه">
            <MyContainer>
                <BigCard title={"تغییرات فرمول محاسبه"} >
                    <BigCard title={"محاسبه خط مبنا"} >
                    <Stack style={{minHeight:120}} direction="row" spacing={1} justifyContent="space-around" alignItems="center" >
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography  >
                                دمای هوای امروز                         
                            </Typography>
                            <Chip label={<PersianNumber number={32+" سانتی‌گراد"} />} variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            رطوبت هوای امروز                        
                            </Typography>
                            <Chip label={<PersianNumber number={50+"%"} />} variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            وضعیت کلی هوا                       
                            </Typography>
                            <Chip label="آفتابی" variant="filled" />
                        </Stack>    
                        <Stack direction="row" spacing={1} alignItems="center" >
                            <Typography>
                            میزان تولید امروز                         
                            </Typography>
                            <Chip label={<PersianNumber number={675000 +" (kwh)"} /> } variant="filled" />
                        </Stack>    
                    </Stack>  
                </BigCard>
                <BigCard title="مبنای محاسبه خط مبنا" >
                    <Grid 
                        justifyContent="flex-start" 
                        direction={"row"} 
                        alignItems="center" 
                        style={{marginTop:12}} 
                        container    
                        >
                        <Typography  >انتخاب</Typography>
                        <TextField 
                            style={{width:80,margin:5}}   
                            type="number"
                            InputProps={{ inputProps: { min: 1 } }}
                            onChange={(e)=>setMabna(prev=>({...prev,days : e.target.value}))}
                            />
                        <Typography>روز از</Typography>
                        <TextField 
                            style={{width:80,margin:5}} 
                            type={"number"}
                            InputProps={{ inputProps: { min: mabna.days } }}
                            onChange={(e)=>setMabna(prev=>({...prev,fromDays : e.target.value}))}  
                            />
                        <Typography>روز</Typography>
                        <InputLabel style={{margin:8,color:'black'}} >برای تجمیع کننده: </InputLabel>
                        <Select
                            style={{width:350,marginLeft:15}}
                            color="primary"
                            onChange={(e)=>setMabna(prev=>({...prev,aggregator : e.target.value}))}
                            >
                            <MenuItem style={{direction:"rtl"}} value={'aggregator'}>تجمیع کننده 1</MenuItem>
                            <MenuItem style={{direction:"rtl"}} value={'aggregator2'}>تجمیع کننده 2</MenuItem>
                        </Select>
                        <Button color="info" onClick={()=>save()} variant="contained"  >ثبت</Button>

                    </Grid>
                </BigCard>
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default FormulaAdmin