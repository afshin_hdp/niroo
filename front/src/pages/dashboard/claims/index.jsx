import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import { getObjectionApi } from "apis/api"
import Table from "components/Table"
import { useState,useEffect } from "react"
import { getUserName } from "utils/getAndSetToken"
import { Button, Typography,Grid , Stack, TextField,Chip} from "@mui/material"
import Modal from "components/Modal"
import PersianNumber from "components/PersianNumber"
const Claim = ()=>{
    const [userName,setUserName] = useState("")
    const [objections,setObjections ] = useState([])
    const [modal,setModal] = useState(false)
    const [message,setMessage] = useState({})
    const getNameOfUser = ()=>{
        setUserName(getUserName())
    }
    useEffect(()=>{
        getNameOfUser()
    },[])
    useEffect(()=>{
        getObjections()
    },[userName])
    const getObjections = async()=>{
        try{
            const res = await getObjectionApi(userName)            
            const arr = res.data.obj.map((ob,i)=>(
                {
                    no:<PersianNumber number={++i}  />,
                    subject:ob.subject,
                    bill:<PersianNumber number={ob.bill}  />,
                    situation : ob.situation === "" ?
                    <Typography color="silver"  variant="contained" >بررسی نشده</Typography> 
                    : ob.situation ? 
                    <Typography color="secondary"  variant="contained" >تایید شده</Typography>
                    :
                    <Typography color="error"  variant="contained" >رد شده</Typography>,

                    details : <Button color="info" onClick={()=>{setModal(true);setMessage(ob)}}  variant="contained" > نمایش متن اعتراض</Button>
                })
                )
            setObjections(arr)
        }catch(e){}
    }

    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'عنوان',field :'subject'},        
        {txt:'قبض',field :'bill'},
        {txt:'وضعیت',field :'situation'},        
        {txt:'متن اعتراض',field :'details'},

    ]

    return (
        <LayOut pageTitle= "اعتراضات">
            <Modal title="پیغام" open={modal} handleClose={()=>{setModal(false);setMessage("")}} >
            <Grid xs={12}  container item direction={"column"} justifyContent={"space-around"} style={{marginTop:15}} >
                <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            عنوان اعتراض                       
                         </Typography>
                        <Chip label={message.subject} variant="filled" />
                    </Stack>    
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            متن اعتراض                       
                         </Typography>
                        <TextField 
                            minRows={4}
                            multiline 
                            value={message.msg} 
                            maxRows={4} 
                            />
                    </Stack>    
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            پاسخ اعتراض                       
                         </Typography>
                        <TextField 
                            minRows={4}
                            multiline                              
                            maxRows={4} 
                            value={message.answer}
                            />
                    </Stack>    
                </Grid>
                <Grid style={{width:350,marginTop:8}} container alignItems={"center"} justifyContent={'flex-end'} >
                    <Button variant="contained" color="info" onClick={()=>{setModal(false);setMessage({})}} >
                        بستن
                    </Button>
                </Grid>
            </Modal>
            <MyContainer>
                <BigCard title={"اعتراضات"} >
                  {
                      objections.length > 0 && (
                           <Table cols={tableCols} rows={objections}  /> 
                      )
                  }
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default Claim