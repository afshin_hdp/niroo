import LayOut from "pages/layOut/layout"
import Chart from 'chart.js/auto';
import { useEffect } from "react";
import { Grid, Typography} from "@mui/material";
import Card from "components/Card"
import AccessAlarmsIcon from '@mui/icons-material/AccessAlarms';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import Battery from '@mui/icons-material/BatteryCharging90';
import PersianNumber from "components/PersianNumber";
import { baseLineApi } from "apis/api";
import BigCard from "components/BigCard"; 
import MyContainer from "components/Container"; 
import { useState } from "react";
import { getUserName } from "utils/getAndSetToken";
const Dashboard = ()=>{
    const [baseLine,setBaseLine] = useState({})
    const [usage,setUsage] = useState('0')
    const [khateMabna,setkhateMabna] = useState('0')
    const [userName,setUserName] = useState("")
    const getNameOfUser = ()=>{
        setUserName(getUserName())
    }
    useEffect(()=>{  
        getNameOfUser()   
        if(Object.keys(baseLine).length !== 0){
        const ctx = document.getElementById('myChart');
        const myChart = new Chart(ctx, {
            type: 'line',
            data : {
                labels: ['1', '2', '3', '4', '5', '6', '7'],
                datasets: [{
                label: 'خط مبنا',
                data: baseLine.baseLine &&  [baseLine.baseLine[20],baseLine.baseLine[21],baseLine.baseLine[22],
                    baseLine.baseLine[23],baseLine.baseLine[24],baseLine.baseLine[25],
                    baseLine.baseLine[26],baseLine.baseLine[27],baseLine.baseLine[28],baseLine.baseLine[29],
                    baseLine.baseLine[30]],
                fill: true,
                borderColor: 'rgb(75, 192, 192)',
                },
                {
                    label: 'مصرف',
                    data: baseLine.usage &&  [baseLine.usage[20],baseLine.usage[21],baseLine.usage[22],
                        baseLine.usage[23],baseLine.usage[24],baseLine.usage[25],
                        baseLine.usage[26],baseLine.usage[27],baseLine.usage[28],baseLine.usage[29],
                        baseLine.usage[30]],
                    fill: true,
                    borderColor: 'gray',
                }]
            },
            options: {
                aspectRatio:4,
                scales: {
                    y: { // defining min and max so hiding the dataset does not change scale range
                    min: 0,
                    max: ((Math.ceil( Math.max( Math.max(baseLine.usage[20],baseLine.usage[21],baseLine.usage[22],
                        baseLine.usage[23],baseLine.usage[24],baseLine.usage[25],
                        baseLine.usage[26],baseLine.usage[27],baseLine.usage[28],baseLine.usage[29],
                        baseLine.usage[30]),Math.max(
                            baseLine.baseLine[20],baseLine.baseLine[21],baseLine.baseLine[22]
                            ,baseLine.baseLine[23],baseLine.baseLine[24],baseLine.baseLine[25],
                    baseLine.baseLine[26],baseLine.baseLine[27],baseLine.baseLine[28],baseLine.baseLine[29],
                    baseLine.baseLine[30]
                        )))  +10000 ) / 1000 ).toFixed() * 1000
                    }
                },
                animations: {
                    tension: {
                    duration: 1000,
                    easing: 'linear',
                    from: 1,
                    to: 0,
                    loop: false
                    }
                },
            }
        });
        return ()=>myChart.destroy();
    }
    },[usage])
    
    const getDataFunction = async()=>{
        if(userName !== ""){
            try{
                const res = await baseLineApi(userName)
                setBaseLine(res.data)
                setUsage(res.data.usage[26])
                setkhateMabna(res.data.baseLine[26])
            }catch(e){}
        }
    } 
    useEffect(()=>{
        getDataFunction()
    },[userName])
    return(
        <LayOut pageTitle="گزارش تجمیعی">
            <MyContainer>
            <BigCard>    
            <Grid container justifyContent="space-around" alignItems="center" >
                <Card 
                    title="حوادث شبکه" 
                    count={<Typography variant="h6" component="span" color="error" >
                        <PersianNumber number={912}  /> 
                        </Typography>}
                    icon={<AccessAlarmsIcon color="error"  />} 
                />
                <Card 
                    title="خط مبنا" 
                    count={
                        <>
                            <Typography>KWh</Typography>
                            <Typography variant="h6" component="span" color="#0288D1" >
                                <PersianNumber number={khateMabna}  />                                 
                            </Typography>                        
                        </>    
                    }
                    icon={<ShowChartIcon color="info"  />} />
                <Card 
                    title="مصرف" 
                    count={
                        <>
                            <Typography>KWh</Typography>
                            <Typography variant="h6" component="span" color="green" >
                                
                                <PersianNumber number={usage}  /> 
                            </Typography>
                        </>
                        }
                    icon={<Battery color="success"  />} 
                    />
            </Grid>
            </BigCard>
            <BigCard title="مقایسه خط مبنا و میزان مصرف" >            
                <canvas style={{marginTop:50}} id="myChart" width="500" height="400"></canvas>
            </BigCard>
            </MyContainer>
        </LayOut>
    )
}
export default Dashboard