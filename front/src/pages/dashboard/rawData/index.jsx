import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import  Table  from "components/Table"
import { useEffect, useState } from "react"
import {baseLineApi, billApi} from "apis/api"
import red from "assets/images/red.png"
import green from "assets/images/green.png"
import { getUserName } from "utils/getAndSetToken"
import PersianNumber from "components/PersianNumber"
const RawData = ()=>{
    const [tableRows,setTableRows] = useState([])
    const [billState,setBillState] = useState(true)

    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'میزان مصرف(به kwh )',field :'expense'},
        {txt:'تاریخ',field :'date'},
    ]
    const [userName,setUserName] = useState("")

    const getNameOfUser = ()=>{
        setUserName(getUserName())
    }

    const getBaseLineUsage = async()=>{
        if(userName !== ""){
        try{
            const res = await baseLineApi(userName)
            let arr = []
            let i = 0
            for(const [key, value] of Object.entries(res.data.usage)){
                arr.push({no : <PersianNumber number={++i}  /> ,expense : <PersianNumber number={value}  />, 
                    date : <PersianNumber number={key+" ماه"}  /> })                                    
            }
            const resBill = await billApi(userName)
            setBillState(resBill.data.bill > 0)
            setTableRows(arr)
        }catch(e){}
        }
    }
    useEffect(()=>{
        getNameOfUser()
    },[])
    useEffect(()=>{
        getBaseLineUsage()
    },[userName])
    return (
        <LayOut pageTitle= "داده های خام">
            <MyContainer>
                <BigCard title={"داده های خام"} >
                    <Table  rows={tableRows} cols={tableCols} />
                </BigCard>
                <BigCard title={"محدوده کاری تجمیع کننده"} >
                    <div style={{display:'flex',justifyContent : 'center'}} >
                    {billState ? (
                      <img src={green} />      
                    ):(
                      <img src={red} />   
                    )}
                    </div>
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default RawData