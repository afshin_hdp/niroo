import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import { MenuItem,Select, InputLabel, Grid,  } from "@mui/material"
import { useEffect,useState } from "react"
import { baseLineApi } from "apis/api"
import { Chart } from "chart.js"
const DifferentAggregators = ()=>{
    const[usage,setUsage] = useState({})
    const [ag,setAg] = useState('')
    const handleChangeChart = (value)=>{
        setAg(value)
    }
    useEffect(()=>{
        if(ag !== '')
        getAggregatorUsage(ag)
    },[ag])
    const getAggregatorUsage = async(x)=>{
        try{
            const res = await baseLineApi(x)
            setUsage(res.data)
        }catch(e){}
    }
    useEffect(()=>{
        if(Object.keys(usage).length > 0){
            const ctx = document.getElementById('usage')
            const chart = new Chart(ctx, {
                type: 'line',
                data : {
                    labels: ['20', '21', '22', '23', '24', '25', '26'],
                    datasets: [{
                        label: 'میزان مصرف',
                        data: [usage.usage[20],usage.usage[21],usage.usage[22],
                        usage.usage[23],usage.usage[24],usage.usage[25],usage.usage[26]],
                        fill: true,
                        borderColor: 'rgb(75, 192, 192)',
                        },{
                            label: 'خط مبنا',
                            data: [usage.baseLine[20],usage.baseLine[21],usage.baseLine[22],
                            usage.baseLine[23],usage.baseLine[24],usage.baseLine[25],usage.baseLine[26]],
                            fill: true,
                            borderColor: 'tomato',
                            borderDash : [10,5]
                                    }
                    ]
                },
                options: {
                    aspectRatio:4,
                    scales: {
                        y: { // defining min and max so hiding the dataset does not change scale range
                        min: 0,
                        max: ((Math.max(usage.usage[20],usage.usage[21],usage.usage[22],usage.usage[23],usage.usage[24],usage.usage[25],
                            usage.usage[26]) + 10000) / 10000).toFixed() *10000                      
                        }
                    },
                    animations: {
                        tension: {
                        duration: 1000,
                        easing: 'linear',
                        from: 1,
                        to: 0,
                        loop: false
                        }
                    },
                }
            });
            return ()=>chart.destroy()
        }
    },[usage])




    return (
        <LayOut pageTitle= "وضعیت توزیع کننده‌ها">
            <MyContainer>
                <BigCard title={"وضعیت توزیع کننده‌ها"} >
                    <Grid container justifyContent="space-between" alignItems="center" >                    
                    <Grid alignItems={"center"} xs={6}  item container justifyContent={"flex-start"}  >
                        <InputLabel style={{marginTop:8}} >انتخاب توزیع کننده: </InputLabel>
                        <Select
                            style={{width:350,marginTop:15}}
                            color="primary"
                            >
                            <MenuItem style={{direction:"rtl"}} value={'aggregator'}> توزیع کننده 1</MenuItem>
                        </Select>
                    </Grid>
                    <Grid alignItems={"center"} xs={6}   item container justifyContent={"flex-start"}  >
                        <InputLabel style={{marginTop:8}} >انتخاب تجمیع کننده: </InputLabel>
                        <Select
                            style={{width:350,marginTop:15}}
                            onChange={(e)=>handleChangeChart(e.target.value)}
                            label="انتخاب تجمیع کننده"
                            placeholder="انتخاب تجمیع کننده"
                            color="primary"
                            >
                            <MenuItem style={{direction:"rtl"}} value={'aggregator'}>تجمیع کننده 1</MenuItem>
                            <MenuItem style={{direction:"rtl"}} value={'aggregator2'}>تجمیع کننده 2</MenuItem>
                        </Select>
                    </Grid>                    
                    </Grid>
                    <canvas width="400" height="300" style={{marginTop:10}} id="usage" ></canvas>    
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default DifferentAggregators