import LayOut from "pages/layOut/layout"
import { useEffect } from "react";
import Chart from 'chart.js/auto';
import MyContainer from "components/Container" 
import BigCard from "components/BigCard" 
import Table from "components/Table"
import { useState } from "react";
import { getUserName } from "utils/getAndSetToken";
import { baseLineApi } from "apis/api";
import PersianNumber from "components/PersianNumber";
const ExpenseRate = ()=>{
    const [userName,setUserName] = useState("")
    const [baseLine,setBaseLine] = useState({})
    const [tableRows,setTableRows] = useState([])
    const getNameOfUser = ()=>{
        setUserName(getUserName())
    }
    useEffect(()=>{
        getNameOfUser()
    },[])
    useEffect(()=>{
        getBaseLine()
    },[userName])
   
    
    const getBaseLine = async()=>{
        try{
            const res = await baseLineApi(userName)
            setBaseLine(res.data.usage) 
            let arr = []
            let i = 0
            for(const [key, value] of Object.entries(res.data.usage)){
                arr.push({no : <PersianNumber number={++i}  /> ,expense : <PersianNumber number={value}  />, date : <PersianNumber number={key+" ماه"}  /> })                                    
            }                        
            setTableRows(arr)           
        }catch(e){}
    }

    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'میزان مصرف(به kwh )',field :'expense'},
        {txt:'تاریخ',field :'date'},
    ]


    useEffect(()=>{  
        if(Object.keys(baseLine).length !== 0){      
        const ctx = document.getElementById('myChart');
        const myChart = new Chart(ctx, {
            type: 'line',
            data : {
                labels: ['1', '2', '3', '4', '5', '6', '7'],
                datasets: [
                {
                    label: 'مصرف',
                    data: baseLine &&  [baseLine[20],baseLine[21],baseLine[22],
                    baseLine[23],baseLine[24],baseLine[25],
                    baseLine[26],baseLine[27],baseLine[28],baseLine[29],
                    baseLine[30]],
                    fill: true,
                    borderColor: 'gray',
                }]
            },
            options: {
                aspectRatio:4,
                scales: {
                    y: { // defining min and max so hiding the dataset does not change scale range
                    min: 0,
                    max: baseLine && ((Math.max( baseLine[20],baseLine[21],baseLine[22],
                    baseLine[23],baseLine[24],baseLine[25],
                    baseLine[26],baseLine[27],baseLine[28],baseLine[29],
                    baseLine[30] )+1000 ) / 1000 ).toFixed() * 1000
                    }
                },
                animations: {
                    tension: {
                    duration: 1000,
                    easing: 'linear',
                    from: 1,
                    to: 0,
                    loop: false
                    }
                },
            }
        });
        return ()=>myChart.destroy()  
    }
    },[baseLine])
    return (
        <LayOut pageTitle="میزان مصرف">
            <MyContainer>
                <BigCard title={"نمودار میزان مصرف"} >
                    <canvas style={{marginTop:50}} id="myChart" width="500" height="400"></canvas>
                </BigCard>
                <BigCard title={"میزان مصرف"} >
                    <Table  rows={tableRows} cols={tableCols} />
                </BigCard>
            </MyContainer>
        </LayOut>
    )
}
export default ExpenseRate