import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import {Typography, Stack, Grid,Chip,Select,MenuItem,InputLabel,CircularProgress, Backdrop} from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import { useEffect } from "react"
import { billApi , baseLineApi, getObjectionApi} from "apis/api"
import { useState } from "react"
import PersianNumber from "components/PersianNumber"
import { Chart } from "chart.js"
import Table from "components/Table"
const DifferentDistAdmin = ()=>{
    const[usage,setUsage] = useState({})
    const [objections,setObjections] = useState([])
    const [loading,setLoading] = useState(false)
    const [bills,setBills ] = useState({bill:'',finalBill:''})
    const[usageAll,setUsageAll] = useState({})
    const [diff,setDiff] = useState(0)
    const [save,setSave] = useState(0)
    const [userName,setUserName] = useState("aggregator")
    const [tableRows,setTableRows] = useState([])
    const [ag,setAg] = useState('aggregator')
    const handleChangeChart = (value)=>{
        setAg(value)
    }
    useEffect(()=>{
        getAggregatorUsage(ag)
    },[ag])
    const getAggregatorUsage = async(x)=>{
        try{
            const res = await baseLineApi(x)
            setUsage(res.data)
        }catch(e){}
    }
    useEffect(()=>{
        if(Object.keys(usage).length > 0){
            const ctx = document.getElementById('usage')
            const chart = new Chart(ctx, {
                type: 'line',
                data : {
                    labels: ['20', '21', '22', '23', '24', '25', '26'],
                    datasets: [{
                        label: 'میزان مصرف',
                        data: [usage.usage[20],usage.usage[21],usage.usage[22],
                        usage.usage[23],usage.usage[24],usage.usage[25],usage.usage[26]],
                        fill: true,
                        borderColor: 'rgb(75, 192, 192)',
                        },{
                            label: 'خط مبنا',
                            data: [usage.baseLine[20],usage.baseLine[21],usage.baseLine[22],
                            usage.baseLine[23],usage.baseLine[24],usage.baseLine[25],usage.baseLine[26]],
                            fill: true,
                            borderColor: 'tomato',
                            borderDash : [10,5]
                                    }
                    ]
                },
                options: {
                    aspectRatio:4,
                    scales: {
                        y: { // defining min and max so hiding the dataset does not change scale range
                        min: 0,
                        max: ((Math.max(usage.usage[20],usage.usage[21],usage.usage[22],usage.usage[23],usage.usage[24],usage.usage[25],
                            usage.usage[26]) + 10000) / 10000).toFixed() *10000                      
                        }
                    },
                    animations: {
                        tension: {
                        duration: 1000,
                        easing: 'linear',
                        from: 1,
                        to: 0,
                        loop: false
                        }
                    },
                }
            });
            return ()=>chart.destroy()
        }
    },[usage])

    
    
    const handleChange = (value)=>{
        setUserName(value)
    }

    useEffect(()=>{
        getAllBills()
    },[userName])

    const getAllBills = async()=>{
        const resBAll = await billApi(userName)
        setBills({bill:resBAll.data.bill,finalBill:resBAll.data.finalBill})
    }
    useEffect(()=>{
        if(bills.bill !== ""){
        setTableRows ([
            {
                no : <PersianNumber number={1}  />,
                status : 'پرداخت‌شده',
                date : <PersianNumber number={'20 ماه'}  />,            
                spend : <PersianNumber number={bills.bill}  />,
                fee :  (bills.bill < 0 ?  
                    <Typography style={{color:'tomato'}} > <PersianNumber number={bills.finalBill}  /> جریمه  </Typography> :
                    <Typography style={{color:'teal'}} > <PersianNumber number={bills.finalBill}  />پاداش</Typography>
                ),
            },
            {
                no : <Typography style={{color:'silver'}} ><PersianNumber number={'2 (فرضی)'}  /> </Typography>,
                status : 'پرداخت‌ نشده',
                date : <PersianNumber number={'21 ماه'}  />,            
                spend : <PersianNumber number={9800000}  />,
                fee : <PersianNumber number={1570}  />,
            },
            {
                no : <Typography style={{color:'silver'}} > <PersianNumber number={'3 (فرضی)'}  /></Typography>,
                status : 'پرداخت‌ نشده',
                date : <PersianNumber number={'22 ماه'}  />,            
                spend : <PersianNumber number={1080000}  />,
                fee : <PersianNumber number={32870}  />,
            }
            ])
        }
    },[bills])
    useEffect(()=>{
        getBill()
    },[])
    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'وضعیت',field :'status'},
        {txt:'تاریخ',field :'date'},        
        {txt:'میزان صرفه‌جویی',field :'spend'},
        {txt:'قبض (به ریال)',field :'fee'},
    ]

    const objectionCols = [
        {txt:'شماره',field :'no'},
        {txt:'عنوان',field :'subject'},        
        {txt:'قبض',field :'bill'},
    ]



    const getBill = async()=>{
        try{
            setLoading(true)
            const res = await billApi("aggregator")
            const res2 = await billApi("aggregator2")
            const resAll = await baseLineApi("all")
            setUsageAll(resAll.data)
            setDiff(((Number(res2.data.bill) + Number(res2.data.bill)) * 100).toFixed(2))
            setSave(((Number(res2.data.bill) + Number(res2.data.bill))))
////
            const resObjection = await getObjectionApi('all')            
            const arr = resObjection.data.obj.map((ob,i)=>(
                {
                    no:<PersianNumber number={++i}  />,
                    subject:ob.subject,
                    bill:<PersianNumber number={ob.bill}  />,
                })
                )
            setObjections(arr)


        }catch(e){}
        finally{
            setLoading(false)
        }
    }
    useEffect(()=>{
        if(Object.keys(usageAll).length > 0){
            const ctx = document.getElementById('usageAll');        
            const chart1 = new Chart(ctx, {
                type: 'line',
                data : {
                    labels: ['20', '21', '22', '23', '24', '25', '26'],
                    datasets: [{
                        label: 'میزان مصرف',
                        data: [usageAll.usage[20],usageAll.usage[21],usageAll.usage[22],
                            usageAll.usage[23],usageAll.usage[24],usageAll.usage[25],usageAll.usage[26]],
                        fill: true,
                        borderColor: 'rgb(75, 192, 192)',
                        },{
                            label: 'خط مبنا',
                            data: [usageAll.baseLine[20],usageAll.baseLine[21],usageAll.baseLine[22],
                                usageAll.baseLine[23],usageAll.baseLine[24],usageAll.baseLine[25],usageAll.baseLine[26]],
                            fill: true,
                            borderColor: 'tomato',
                            borderDash : [10,5]
                                    }
                    ]
                },
                options: {
                    aspectRatio:4,
                    scales: {
                        y: { // defining min and max so hiding the dataset does not change scale range
                        min: 0,
                        max: ((Math.max(usageAll.usage[20],usageAll.usage[21],usageAll.usage[22],usageAll.usage[23],usageAll.usage[24],usageAll.usage[25],
                            usageAll.usage[26]) + 10000) / 10000).toFixed() *10000                      
                        }
                    },
                    animations: {
                        tension: {
                        duration: 1000,
                        easing: 'linear',
                        from: 1,
                        to: 0,
                        loop: false
                        }
                    },
                }
            });
            return ()=>chart1.destroy()
        }
    },[usageAll])

    return (
        <LayOut pageTitle= "گزارش تجمیعی">
            <MyContainer>
                <BigCard title={"گزارش تجمیعی"} >
                    <Grid style={{marginTop:15}} container justifyContent={"space-between"} alignItems={"center"} >
                    <Card sx={{ minWidth: 155,borderRadius:3 }}>
                        <CardContent style={{height:140}} >
                            <Typography color="darkorchid" sx={{ fontSize: 16,display:'flex',justifyContent:'center' }}  >
                                صرفه‌جویی ریالی
                            </Typography>
                            <Typography style={{marginTop:20}} sx={{fontSize: 18,display:'flex',justifyContent:'center' }} >
                                {diff}
                            </Typography>
                            <Grid style={{marginTop:20}} container justifyContent={"space-between"} >
                                <Typography  sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    از هفته گذشته
                                </Typography>
                                <Typography  color={'error'} sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    ?<KeyboardArrowDownIcon />
                                </Typography>
                            </Grid>    
                        </CardContent>
                    </Card>
                    <Card sx={{ minWidth: 155,borderRadius:3 }}>
                        <CardContent style={{height:140}} >
                            <Typography color="darkorchid" sx={{ fontSize: 16,display:'flex',justifyContent:'center' }}  >
                                مترمکعب گاز معادل
                            </Typography>
                            <Typography style={{marginTop:20}} sx={{fontSize: 18,display:'flex',justifyContent:'center' }} >
                                {2,315}
                            </Typography>
                            <Grid style={{marginTop:20}} container justifyContent={"space-between"} >
                                <Typography  sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    از هفته گذشته
                                </Typography>
                                <Typography  color={'error'} sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    ?<KeyboardArrowDownIcon />
                                </Typography>
                            </Grid>    
                        </CardContent>
                    </Card>
                    <Card sx={{ minWidth: 155,borderRadius:3 }}>
                        <CardContent style={{height:140}} >
                            <Typography color="darkorchid" sx={{ fontSize: 16,display:'flex',justifyContent:'center' }}  >
                                بشکه نفت معادل
                            </Typography>
                            <Typography style={{marginTop:20}} sx={{fontSize: 18,display:'flex',justifyContent:'center' }} >
                                {4,567}
                            </Typography>
                            <Grid style={{marginTop:20}} container justifyContent={"space-between"} >
                                <Typography  sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    از هفته گذشته
                                </Typography>
                                <Typography  color={'error'} sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    12<KeyboardArrowDownIcon />
                                </Typography>
                            </Grid>    
                        </CardContent>
                    </Card>
                    <Card sx={{ minWidth: 155,borderRadius:3 }}>
                        <CardContent style={{height:140}} >
                            <Typography color="darkorchid" sx={{ fontSize: 16,display:'flex',justifyContent:'center' }}  >
                                درخت معادل
                            </Typography>
                            <Typography style={{marginTop:20}} sx={{fontSize: 18,display:'flex',justifyContent:'center' }} >
                                {2,500}
                            </Typography>
                            <Grid style={{marginTop:20}} container justifyContent={"space-between"} >
                                <Typography  sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    از هفته گذشته
                                </Typography>
                                <Typography  color={'error'} sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    ?<KeyboardArrowDownIcon />
                                </Typography>
                            </Grid>    
                        </CardContent>
                    </Card>
                    <Card sx={{ minWidth: 155,borderRadius:3 }}>
                        <CardContent style={{height:140}} >
                            <Typography color="darkorchid" sx={{ fontSize: 16,display:'flex',justifyContent:'center' }}  >
                                صرفه جویی در مصرف   
                            </Typography>
                            <Typography style={{marginTop:20}} sx={{fontSize: 18,display:'flex',justifyContent:'center' }} >
                                {save}
                            </Typography>
                            <Grid style={{marginTop:20}} container justifyContent={"space-between"} >
                                <Typography  sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    از هفته گذشته
                                </Typography>
                                <Typography  color={'error'} sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    3<KeyboardArrowDownIcon />
                                </Typography>
                            </Grid>    
                        </CardContent>
                    </Card>
                    <Card sx={{ minWidth: 155,borderRadius:3 }}>
                        <CardContent style={{height:140}} >
                            <Typography color="darkorchid" sx={{ fontSize: 16,display:'flex',justifyContent:'center' }}  >
                            تعداد نیروگاه متوسط
                            </Typography>
                            <Typography style={{marginTop:20}} sx={{fontSize: 18,display:'flex',justifyContent:'center' }} >
                                {2,500}
                            </Typography>
                            <Grid style={{marginTop:20}} container justifyContent={"space-between"} >
                                <Typography  sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    از هفته گذشته
                                </Typography>
                                <Typography  color={'error'} sx={{fontSize: 12,display:'flex',justifyContent:'center' }} >
                                    4<KeyboardArrowDownIcon />
                                </Typography>
                            </Grid>    
                        </CardContent>
                    </Card>
                    </Grid>
                </BigCard>
                <BigCard title={"محاسبه خط مبنا"} >
                <Stack style={{minHeight:120}} direction="row" spacing={1} justifyContent="space-around" alignItems="center" >
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography  >
                            دمای هوای امروز                         
                        </Typography>
                        <Chip label={<PersianNumber number={32+" سانتی‌گراد"} />} variant="filled" />
                    </Stack>    
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography>
                        رطوبت هوای امروز                        
                        </Typography>
                        <Chip label={<PersianNumber number={50+"%"} />} variant="filled" />
                    </Stack>    
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography>
                        وضعیت کلی هوا                       
                         </Typography>
                        <Chip label="آفتابی" variant="filled" />
                    </Stack>    
                    <Stack direction="row" spacing={1} alignItems="center" >
                        <Typography>
                        میزان تولید امروز                         
                        </Typography>
                        <Chip label={<PersianNumber number={675000 +" (kwh)"} /> } variant="filled" />
                    </Stack>    
                </Stack>  
            </BigCard>
            <BigCard title="میزان کل مصرف و خط مبنا" >
                <canvas width="500" height="450" style={{marginTop:10}} id="usageAll" ></canvas>
            </BigCard>
            <BigCard title={"محاسبات مالی"} >
                <Grid alignItems={"center"}   item container justifyContent={"flex-start"}  >
                    <InputLabel style={{marginTop:8}} >انتخاب تجمیع کننده: </InputLabel>
                    <Select
                        style={{width:350,marginTop:15}}
                        onChange={(e)=>handleChange(e.target.value)}
                        label="انتخاب تجمیع کننده"
                        placeholder="انتخاب تجمیع کننده"
                        color="primary"
                        >
                        <MenuItem style={{direction:"rtl"}} value={'aggregator'}>تجمیع کننده 1</MenuItem>
                        <MenuItem style={{direction:"rtl"}} value={'aggregator2'}>تجمیع کننده 2</MenuItem>
                    </Select>
                </Grid>
                <Grid style={{minHeight:240}} >
                    <Table  rows={tableRows} cols={tableCols} />
                </Grid>
            </BigCard>
            <BigCard title={"لیست اعتراضات"} >
                <Backdrop
                    sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={loading}                    
                    >
                    <CircularProgress color="inherit" />
                </Backdrop>
                  {
                      objections.length > 0 && (
                           <Table cols={objectionCols} rows={objections}  /> 
                      )
                  }
                </BigCard>
                <BigCard title={"نمودار مصرف و خط مبنا به تفکیک تجمیع کننده"} >
                    <Grid alignItems={"center"}   item container justifyContent={"flex-start"}  >
                        <InputLabel style={{marginTop:8}} >انتخاب تجمیع کننده: </InputLabel>
                        <Select
                            style={{width:350,marginTop:15}}
                            onChange={(e)=>handleChangeChart(e.target.value)}
                            label="انتخاب تجمیع کننده"
                            placeholder="انتخاب تجمیع کننده"
                            color="primary"
                            >
                            <MenuItem style={{direction:"rtl"}} value={'aggregator'}>تجمیع کننده 1</MenuItem>
                            <MenuItem style={{direction:"rtl"}} value={'aggregator2'}>تجمیع کننده 2</MenuItem>
                        </Select>
                    </Grid>
                    <Grid style={{minHeight:240}} >
                    <canvas width="400" height="300" style={{marginTop:10}} id="usage" ></canvas>
                    </Grid>
                </BigCard>  
            </MyContainer>
        </LayOut>
        )
}
export default DifferentDistAdmin