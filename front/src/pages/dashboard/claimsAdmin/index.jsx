import LayOut from "pages/layOut/layout"
import MyContainer from "components/Container"
import BigCard from "components/BigCard"
import { useState,useEffect  } from "react"
import { getObjectionApi, answerToObjectionApi } from "apis/api"
import Table from "components/Table"
import { Button,Grid,Chip,Typography, Stack, TextField } from "@mui/material"
import Modal from "components/Modal"
const ClaimsAdmin = ()=>{
    const [objections,setObjections] = useState([])
    const [modal,setModal] = useState(false)
    const [selectedObjection,setSelectedObjection] = useState({})
    const [answer,setAnswer] = useState('')
    useEffect(()=>{
        getAllObjections()
    },[])
    const showModal = (obj)=>{   
        setSelectedObjection(obj)
        setModal(true)
    }
    const getAllObjections = async()=>{
        try{
            const res = await getObjectionApi("all")
            const Arr = []
            const obj = res.data.obj;
            
            for(let i = 0 ; i < obj.length; i++){
                Arr.push({
                    no: i + 1,
                    subject : obj[i].subject,
                    bill : obj[i].bill,
                    aggregator : obj[i].aggregator,
                    situation : obj[i].situation === "" ?
                     <Typography color="silver"  variant="contained" >بررسی نشده</Typography> 
                     : obj[i].situation ? 
                     <Typography color="secondary"  variant="contained" >تایید شده</Typography>
                     :
                     <Typography color="error"  variant="contained" >رد شده</Typography>,
                    details : <Button onClick={()=>showModal(obj[i])} color="info" variant="contained" >بررسی</Button>
                })
            }

            setObjections(Arr)
        }catch(e){}
    }
    const tableCols = [
        {txt:'شماره',field :'no'},
        {txt:'عنوان',field :'subject'},        
        {txt:'قبض',field :'bill'},
        {txt:'تجمیع کننده',field :'aggregator'},
        {txt:'وضعیت',field :'situation'},
        {txt:'بررسی',field :'details'},
    ]

    const updateObjection = async(isConfirm)=>{
        try{
            const sendObj = {...selectedObjection,situation:isConfirm, answer :answer,user:selectedObjection.aggregator }
            await answerToObjectionApi(sendObj)
            getAllObjections()
            setModal(false)
        }catch(e){}
    }

    return (
        <LayOut pageTitle= "مشاهده اعتراض‌ها">
            <Modal open={modal} title="بررسی اعتراض محاسبات مالی" 
                handleClose={()=>{setModal(false);setSelectedObjection({})}} 
                >
                <Grid xs={12}  container item direction={"column"} justifyContent={"space-around"} style={{marginTop:15}} >
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-around"} >
                        <Typography>
                        نام شرکت توزیع نیروی برق                       
                         </Typography>
                        <Chip label="شرکت توزیع نیروی برق ۱" variant="filled" />
                    </Stack>    
                    <Stack 
                        direction="row" 
                        style={{marginTop:5}} 
                        spacing={1} 
                        alignItems="center" 
                        justifyContent={"space-between"}
                        >
                        <Typography>
                        نام تجمیع کننده                       
                         </Typography>
                        <Chip label={selectedObjection.aggregator} variant="filled" />
                    </Stack>    
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            عنوان اعتراض                       
                         </Typography>
                        <Chip label={selectedObjection.subject} variant="filled" />
                    </Stack>    
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            متن اعتراض                       
                         </Typography>
                        <TextField 
                            minRows={4}
                            multiline 
                            value={selectedObjection.msg} 
                            maxRows={4} 
                            />
                    </Stack>    
                    <Stack direction="row" style={{marginTop:5}} spacing={1} alignItems="center" justifyContent={"space-between"} >
                        <Typography>
                            توضیحات                       
                         </Typography>
                        <TextField 
                            onChange={(e)=>setAnswer(e.target.value)}
                            minRows={4}
                            multiline                              
                            maxRows={4} 
                            />
                    </Stack>    
                </Grid>                    
                <Grid xs={12}  container item justifyContent={"space-around"} style={{marginTop:15}} >
                    <Button onClick={()=>updateObjection(true)} variant="contained" color="success" >تایید </Button>
                    <Button onClick={()=>updateObjection(false)} variant="contained" color="error" >رد </Button>                    
                </Grid>
            </Modal>
            <MyContainer>
                <BigCard title={"مشاهده اعتراض‌ها"} >
                    {objections.length > 0  && (<Table cols={tableCols} rows={objections}  /> ) }
                </BigCard>
            </MyContainer>
        </LayOut>
        )
}
export default ClaimsAdmin