const express = require('express')
const app = express()
var cors = require('cors')
const port = 3000
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: '100kb' }));
app.use(cors())

const bodyParser = require("body-parser");
let jsonParser = bodyParser.json()

let ID;
let formula1;

app.listen(port, async () => {
    console.log(`Example app listening at http://localhost:${port}`);
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        dbo.createCollection("users", function (err, res) {
            console.log("users collection created!");

        });

        dbo.collection("baseLineTest").drop(function (err, delOK) {
            if (delOK) console.log("Collection deleted");
        });
        dbo.createCollection("testData", function (err, res) {
            console.log("testData collection created!");

        });
        dbo.createCollection("formula", function (err, res) {
            console.log("formula collection created!");

        });
        dbo.createCollection("objection", function (err, res) {
            console.log("objection collection created!");

        });
        dbo.createCollection("notification", function (err, res) {
            console.log("notification collection created!");

        })
    })

});


app.post('/login', cors(), async (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        dbo.collection("users").findOne({ username: `${username}`, password: `${password}` }, function (err, result) {
            let panel;
            if (err) throw err;
            try {
                console.log(result.panel);
                panel = result.panel;
                res.json({ "matched": "true", "panel": panel })
                db.close();
            } catch (err) {
                res.json({ "err": result })
            }

        });
    });



})

app.post('/register', jsonParser, async (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let panelName = req.body.panelName;

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        var myobj = { username: username, password: password, panel: panelName };
        dbo.collection("users").insertOne(myobj, function (err, res) {
            if (err) throw err;
            console.log("1 document inserted");
            console.log(res)
            db.close();
        });
    });
    res.json({ "userInserted": "true" })


})


let ID1use = {
    20: '',
    21: '',
    22: '',
    23: '',
    24: '',
    25: '',
    26: '',
    27: '0',
    28: '0',
    29: "0",
    30: '0'
}

let ID2use = {
    20: '',
    21: '',
    22: '',
    23: '',
    24: '',
    25: '',
    26: '',
    27: '0',
    28: '0',
    29: "0",
    30: '0'
}



var ID1total = {
    20: '',
    21: '',
    22: '',
    23: '',
    24: '',
    25: '',
    26: '',
    27: "0",
    28: "0",
    29: "0",
    30: "0"

}


var ID2total = {
    20: '',
    21: '',
    22: '',
    23: '',
    24: '',
    25: '',
    26: '',
    27: "0",
    28: "0",
    29: "0",
    30: "0"

}




app.post('/setData', cors(), (req, res) => {
    ID = req.body.ID
    let data = req.body.id
    ID = parseInt(ID)
    console.log("ID", ID)
    console.log("data[0].use", data[0].use)
    if (ID == 2) {
        ID2total[20] = parseInt(data[0].use) + parseInt(data[0].use1011)
        console.log("ID2total[20]", ID2total[20])
        ID2total[21] = parseInt(data[1].use) + parseInt(data[1].use1011)
        ID2total[22] = parseInt(data[2].use) + parseInt(data[2].use1011)
        ID2total[23] = parseInt(data[3].use) + parseInt(data[3].use1011)
        ID2total[24] = parseInt(data[4].use) + parseInt(data[4].use1011)
        ID2total[25] = parseInt(data[5].use) + parseInt(data[5].use1011)
        ID2total[26] = parseInt(data[6].use) + parseInt(data[6].use1011)
        ID2use[20] = parseInt(data[0].use)
        ID2use[21] = parseInt(data[1].use)
        ID2use[22] = parseInt(data[2].use)
        ID2use[23] = parseInt(data[3].use)
        ID2use[24] = parseInt(data[4].use)
        ID2use[25] = parseInt(data[5].use)
        ID2use[26] = parseInt(data[6].use)
    }

    if (ID == 1) {
        ID1total[20] = parseInt(data[0].use) + parseInt(data[0].use1011)
        console.log("ID1total[20]", ID1total[20])
        ID1total[21] = parseInt(data[1].use) + parseInt(data[1].use1011)
        ID1total[22] = parseInt(data[2].use) + parseInt(data[2].use1011)
        ID1total[23] = parseInt(data[3].use) + parseInt(data[3].use1011)
        ID1total[24] = parseInt(data[4].use) + parseInt(data[4].use1011)
        ID1total[25] = parseInt(data[5].use) + parseInt(data[5].use1011)
        ID1total[26] = parseInt(data[6].use) + parseInt(data[6].use1011)
        ID1use[20] = parseInt(data[0].use)
        console.log("ID1use[20]", ID1use[20])
        ID1use[21] = parseInt(data[1].use)
        ID1use[22] = parseInt(data[2].use)
        ID1use[23] = parseInt(data[3].use)
        ID1use[24] = parseInt(data[4].use)
        ID1use[25] = parseInt(data[5].use)
        ID1use[26] = parseInt(data[6].use)
    }



    res.json({ "success": true })
})

var finalTotal = {

}

function a() {
    finalTotal = {
        20: `${parseInt(ID1total[20]) + parseInt(ID2total[20])}`,
        21: `${parseInt(ID1total[21]) + parseInt(ID2total[21])}`,
        22: `${parseInt(ID1total[22]) + parseInt(ID2total[22])}`,
        23: `${parseInt(ID1total[23]) + parseInt(ID2total[23])}`,
        24: `${parseInt(ID1total[24]) + parseInt(ID2total[24])}`,
        25: `${parseInt(ID1total[25]) + parseInt(ID2total[25])}`,
        26: `${parseInt(ID1total[26]) + parseInt(ID2total[26])}`,
        27: "0",
        28: "0",
        29: "0",
        30: "0"
    }
}



let ID1baseLine = {

}

let ID2baseLine = {

}

let allBaseLine = {
}

function b() {
    ID1baseLine = {
        20: "0",
        21: `${(ID1use[20]) / 3}`,
        22: `${(ID1use[21] + ID1use[20]) / 3}`,
        23: `${(ID1use[22] + ID1use[21] + ID1use[20]) / 3}`,
        24: `${(ID1use[23] + ID1use[22] + ID1use[21]) / 3}`,
        25: `${(ID1use[24] + ID1use[23] + ID1use[22]) / 3}`,
        26: `${(ID1use[25] + ID1use[24] + ID1use[23]) / 3}`,
        27: `${(ID1use[26] + ID1use[25] + ID1use[24]) / 3}`,
        28: `${(ID1use[26] + ID1use[25]) / 3}`,
        29: `${(ID1use[26]) / 3}`,
        30: "0"

    }

    ID2baseLine = {
        20: "0",
        21: `${(ID2use[20]) / 3}`,
        22: `${(ID2use[21] + ID2use[20]) / 3}`,
        23: `${(ID2use[22] + ID2use[21] + ID2use[20]) / 3}`,
        24: `${(ID2use[23] + ID2use[22] + ID2use[21]) / 3}`,
        25: `${(ID2use[24] + ID2use[23] + ID2use[22]) / 3}`,
        26: `${(ID2use[25] + ID2use[24] + ID2use[23]) / 3}`,
        27: `${(ID2use[26] + ID2use[25] + ID2use[24]) / 3}`,
        28: `${(ID2use[26] + ID2use[25]) / 3}`,
        29: `${(ID2use[26]) / 3}`,
        30: "0"

    }

    allBaseLine = {
        20: `${parseInt(ID1baseLine[20]) + parseInt(ID2baseLine[20])}`,
        21: `${parseInt(ID1baseLine[21]) + parseInt(ID2baseLine[21])}`,
        22: `${parseInt(ID1baseLine[22]) + parseInt(ID2baseLine[22])}`,
        23: `${parseInt(ID1baseLine[23]) + parseInt(ID2baseLine[23])}`,
        24: `${parseInt(ID1baseLine[24]) + parseInt(ID2baseLine[24])}`,
        25: `${parseInt(ID1baseLine[25]) + parseInt(ID2baseLine[25])}`,
        26: `${parseInt(ID1baseLine[26]) + parseInt(ID2baseLine[26])}`,
        27: `${parseInt(ID1baseLine[27]) + parseInt(ID2baseLine[27])}`,
        28: `${parseInt(ID1baseLine[28]) + parseInt(ID2baseLine[28])}`,
        29: `${parseInt(ID1baseLine[29]) + parseInt(ID2baseLine[29])}`,
        30: `${parseInt(ID1baseLine[30]) + parseInt(ID2baseLine[30])}`
    }
}

let ID1BILL;

var ID1Bill = {
}

let ID2BILL;


var ID2Bill = {
}


var allBill = {
}

function bb() {
    ID1BILL =
        (parseInt(ID1baseLine[20]) - parseInt(ID1total[20])) +
        (parseInt(ID1baseLine[21]) - parseInt(ID1total[21])) +
        (parseInt(ID1baseLine[22]) - parseInt(ID1total[22])) +
        (parseInt(ID1baseLine[23]) - parseInt(ID1total[23])) +
        (parseInt(ID1baseLine[24]) - parseInt(ID1total[24])) +
        (parseInt(ID1baseLine[25]) - parseInt(ID1total[25])) +
        (parseInt(ID1baseLine[26]) - parseInt(ID1total[26])) +
        (parseInt(ID1baseLine[27]) - parseInt(ID1total[27])) +
        (parseInt(ID1baseLine[28]) - parseInt(ID1total[28])) +
        (parseInt(ID1baseLine[29]) - parseInt(ID1total[29])) +
        (parseInt(ID1baseLine[30]) - parseInt(ID1total[30]));

    ID1Bill = {
        bill: `${(parseInt(ID1baseLine[20]) - parseInt(ID1total[20])) +
            (parseInt(ID1baseLine[21]) - parseInt(ID1total[21])) +
            (parseInt(ID1baseLine[22]) - parseInt(ID1total[22])) +
            (parseInt(ID1baseLine[23]) - parseInt(ID1total[23])) +
            (parseInt(ID1baseLine[24]) - parseInt(ID1total[24])) +
            (parseInt(ID1baseLine[25]) - parseInt(ID1total[25])) +
            (parseInt(ID1baseLine[26]) - parseInt(ID1total[26])) +
            (parseInt(ID1baseLine[27]) - parseInt(ID1total[27])) +
            (parseInt(ID1baseLine[28]) - parseInt(ID1total[28])) +
            (parseInt(ID1baseLine[29]) - parseInt(ID1total[29])) +
            (parseInt(ID1baseLine[30]) - parseInt(ID1total[30]))
            }`,
        finalBill: `${parseInt(ID1BILL) * 100}`
    }

    ID2BILL =
        (parseInt(ID2baseLine[20]) - parseInt(ID2total[20])) +
        (parseInt(ID2baseLine[21]) - parseInt(ID2total[21])) +
        (parseInt(ID2baseLine[22]) - parseInt(ID2total[22])) +
        (parseInt(ID2baseLine[23]) - parseInt(ID2total[23])) +
        (parseInt(ID2baseLine[24]) - parseInt(ID2total[24])) +
        (parseInt(ID2baseLine[25]) - parseInt(ID2total[25])) +
        (parseInt(ID2baseLine[26]) - parseInt(ID2total[26])) +
        (parseInt(ID2baseLine[27]) - parseInt(ID2total[27])) +
        (parseInt(ID2baseLine[28]) - parseInt(ID2total[28])) +
        (parseInt(ID2baseLine[29]) - parseInt(ID2total[29])) +
        (parseInt(ID2baseLine[30]) - parseInt(ID2total[30]));


    ID2Bill = {
        bill: `${(parseInt(ID2baseLine[20]) - parseInt(ID2total[20])) +
            (parseInt(ID2baseLine[21]) - parseInt(ID2total[21])) +
            (parseInt(ID2baseLine[22]) - parseInt(ID2total[22])) +
            (parseInt(ID2baseLine[23]) - parseInt(ID2total[23])) +
            (parseInt(ID2baseLine[24]) - parseInt(ID2total[24])) +
            (parseInt(ID2baseLine[25]) - parseInt(ID2total[25])) +
            (parseInt(ID2baseLine[26]) - parseInt(ID2total[26])) +
            (parseInt(ID2baseLine[27]) - parseInt(ID2total[27])) +
            (parseInt(ID2baseLine[28]) - parseInt(ID2total[28])) +
            (parseInt(ID2baseLine[29]) - parseInt(ID2total[29])) +
            (parseInt(ID2baseLine[30]) - parseInt(ID2total[30]))
            }`,
        finalBill: `${parseInt(ID2BILL) * 100}`
    }


    allBill = {
        bill: `${parseInt(ID2Bill.bill) + parseInt(ID1Bill.bill)}`,
        finalBill: `${parseInt(ID2Bill.finalBill) + parseInt(ID1Bill.finalBill)}`
    }
}


app.get("/baseLine/:aggregator", cors(), (req, res) => {
    let aggregator = req.params.aggregator
    a()
    b()
    if (aggregator == "all") {

        res.json({
            "baseLine": allBaseLine,
            "usage": finalTotal
        })
    }
    if (aggregator == 'aggregator') {

        res.json({
            "baseLine": ID1baseLine,
            "usage": ID1total
        })


    } else {

        res.json({
            "baseLine": ID2baseLine,
            "usage": ID2total
        })

    }

})

app.get("/bill/:aggregator", cors(), (req, res) => {
    let aggregator = req.params.aggregator
    bb()
    if (aggregator == "all") {
        res.json({
            "bill1": ID1Bill,
            "bill2": ID2Bill,
            "all": allBill
        })
    }
    if (aggregator == "aggregator") {
        res.json(ID1Bill)
    } else {
        res.json(ID2Bill)
    }

})


let objSubject = "objection"
let objMsg = "afshin"
let objBill = 4580000.02

let notSubject = "notification"
let notMsg = "afshin2"

app.post("/set-objection", (req, res) => {
    let subject = req.body.subject;
    let msg = req.body.msg;
    let bill = req.body.bill
    let agg = req.body.user
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        var myobj = { subject: subject, msg: msg, bill: bill, aggregator: agg, answer: "", situation: "" };
        dbo.collection("objection").insertOne(myobj, function (err, res) {
            if (err) throw err;
            console.log("1 document inserted");
            console.log(res)
            db.close();
        });
    });
    res.json({ "objectionInserted": "true" })
})

app.get("/get-notifications/", cors(), (req, res) => {

    res.json({ "subject": notSubject, msg: notMsg })
})

app.post("/count-aggregators", (req, res) => {
    let panel = req.body.panel;
    let usernames = []
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        dbo.collection("users").find({ panel: panel }).toArray(function (err, result) {
            if (err) throw err;
            console.log(result);
            for (i = 0; i < result.length; i++) {
                usernames.push(result[i].username)
            }

            res.json({ "users": usernames })
            db.close();
        });
    });
})


app.post("/formula", (req, res) => {
    formula1 = req.body.formula1;
    let formula2 = req.body.formula2;
    let agg = req.body.aggregator
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        var myobj = { ind1: formula1, ind2: formula2, aggregator: agg };
        dbo.collection("formula").insertOne(myobj, function (err, result) {
            if (err) throw err;
            console.log("1 document inserted");
            console.log(res)
            res.json({ "formulaInserted": true })
            db.close();
        });
    });

})

app.post("/answer-to-objection", (req, res) => {
    let subject = req.body.subject;
    let msg = req.body.msg;
    let bill = req.body.bill;
    let agg = req.body.user;
    let answer = req.body.answer;
    let situation = req.body.situation;

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        var myquery = { subject: subject, msg: msg, bill: bill, aggregator: agg };
        var newvalues = { $set: { answer: answer, situation: situation } };
        dbo.collection("objection").updateOne(myquery, newvalues, function (err, result) {
            if (err) throw err;
            console.log("1 document updated");

            res.json({ "answerInserted": true })
            db.close();
        });
    });

})



app.get("/get-objections/:aggregator", cors(), (req, res) => {
    let agg = req.params.aggregator;
    if (agg == "aggregator" || agg == "aggregator2") {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("objection").find({ aggregator: agg }).toArray(function (err, result) {
                if (err) throw err;
                console.log(result);
                res.json({ "obj": result })
                db.close();
            });
        });
    } else if (agg == "all") {

        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("objection").find({}).toArray(function (err, result) {
                if (err) throw err;
                console.log(result);
                res.json({ "obj": result })
                db.close();
            });
        });

    }

})